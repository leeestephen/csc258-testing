module aluregister(SW, KEY, LEDR, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5); // Top module, controlling all submodules
	input [9:0] SW; 
	// SW3-0: A
	// SW9: reset_n  
	// SW7-5: alu_function
	input [0:0] KEY; 
	// KEY0: clk
	output [7:0] LEDR;
	// LEDR7-0: ALUout
	output [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5;
	// HEX0: A
	// HEX3-1: Off
	// HEX4: Least significant (right hand side)
	// HEX5: most significant (left hand side)
	// {HEX4, HEX5} = ALUout[7:0]
	wire [7:0] aluout, regout;
	
	alu alu0(.A(SW[3:0]), .B(regout[3:0]), .ALUout(aluout), .alu_function(SW[7:5]));
	dflipflop reg0(.d(aluout), .q(regout), .clk(KEY[0]), .reset_n(SW[9]));

	// LEDR output
	assign LEDR = regout;	
	// Hexdisplay output
	hexdisplay hx0 (.hex_digit(SW[3:0]), .segments(HEX0[6:0])); // A
	hexdisplay hx1 (.hex_digit(4'b0000), .segments(HEX1[6:0])); // Off
	hexdisplay hx2 (.hex_digit(4'b0000), .segments(HEX2[6:0])); // Off
	hexdisplay hx3 (.hex_digit(4'b0000), .segments(HEX3[6:0])); // Off
	hexdisplay hx4 (.hex_digit(regout[3:0]), .segments(HEX4[6:0])); // Least significant 4 bits
	hexdisplay hx5 (.hex_digit(regout[7:4]), .segments(HEX5[6:0])); // Most significant 4 bits
endmodule

// alu without register
module alu(A, B, ALUout, alu_function);
	input [3:0] A, B; // 2 4-bit digits
	input [2:0] alu_function; // 3-bit function control input
	output [7:0] ALUout; // 8-bit result output
	reg [7:0] ALUout; // always block register
	wire case0carry, case1carry;
	wire [3:0] case0out, case1out;

	ripplecarry r0(.A(A), .B(4'b0001), .ci(1'b0), .co(case0carry), .S(case0out)); // case 0 adder
	ripplecarry r1(.A(A), .B(B), .ci(1'b0), .co(case1carry), .S(case1out)); // case 1 adder

	// always block for function selection
	always @(*)
	begin
		case(alu_function)
			3'b000: ALUout = {3'b000, case0carry, case0out}; // case 0
			3'b001: ALUout = {3'b000, case1carry, case1out}; // case 1
			3'b010: ALUout = A + B; // case 2
			3'b011: ALUout = {A | B, A ^ B}; // case 3
			3'b100: ALUout = {7'b0000000, A[3] | A[2] | A[1] | A[0] | B[3] | B[2] | B[1] | B[0]}; // case 4
			3'b101: ALUout = B << A; // case 5
			3'b110: ALUout = B >> A; // case 6
			3'b111: ALUout = A * B; // case 7
			default: ALUout = 8'b00000000; // default case
		endcase
	end
endmodule

// 8-bit positive edge-triggered flip-flop, with active-low, sync reset.
module dflipflop(d, q, clk, reset_n);
	input [7:0] d;
	input clk;
	input reset_n;
	output [7:0] q;
	reg [7:0] q; // almost forget this line!

	always @(posedge clk) // triggered every time when clock rises
	begin
		if (reset_n == 1'b0) // when reset_n is 0, output 0
			q <= 0;
		else                 // when reset_n is 1, output 8-bit d
			q <= d;
	end
endmodule

// ripple carry 4-bit adder
module ripplecarry(A, B, ci, co, S);
	input [3:0] A, B; // 2 4-bit digit to be added
	input ci; // input carry bit (0)
	output co; // output carry bit
	output [3:0] S; // sum
	wire c0, c1, c2;
	
	fulladder fa0(.a(A[0]), .b(B[0]), .cin(ci), .cout(c0), .s(S[0]));	
	fulladder fa1(.a(A[1]), .b(B[1]), .cin(c0), .cout(c1), .s(S[1]));	
	fulladder fa2(.a(A[2]), .b(B[2]), .cin(c1), .cout(c2), .s(S[2]));	
	fulladder fa3(.a(A[3]), .b(B[3]), .cin(c2), .cout(co), .s(S[3]));
endmodule

// Full adder
module fulladder(a, b, cin, cout, s);
	input a;
	input b;
	input cin;
	output cout;
	output s;

	assign s = cin ^ (a ^ b);
	assign cout = ((a ^ b) & cin) | (~(a ^ b) & b);
endmodule


// Hexdisplay simplified
module hexdisplay(hex_digit, segments);
    input [3:0] hex_digit;
    output reg [6:0] segments;
   
    always @(*)
        case (hex_digit)
            4'h0: segments = 7'b100_0000;
            4'h1: segments = 7'b111_1001;
            4'h2: segments = 7'b010_0100;
            4'h3: segments = 7'b011_0000;
            4'h4: segments = 7'b001_1001;
            4'h5: segments = 7'b001_0010;
            4'h6: segments = 7'b000_0010;
            4'h7: segments = 7'b111_1000;
            4'h8: segments = 7'b000_0000;
            4'h9: segments = 7'b001_1000;
            4'hA: segments = 7'b000_1000;
            4'hB: segments = 7'b000_0011;
            4'hC: segments = 7'b100_0110;
            4'hD: segments = 7'b010_0001;
            4'hE: segments = 7'b000_0110;
            4'hF: segments = 7'b000_1110;   
            default: segments = 7'b111_1111;
        endcase
endmodule
oh i didnt copy the edited version from the lab computer..
it's the same as the old one
i think the major difference is we changed the name of those regs