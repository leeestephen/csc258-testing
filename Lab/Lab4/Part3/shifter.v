module shifter_main(SW, KEY, LEDR);
	input [9:0] SW;
	// SW7-0: LoadVal7-0
	// SW9: reset_n
	input [3:0] KEY;
	// KEY0: clk
	// KEY1: Load_n
	// KEY2: ShiftRight
	// KEY3: ASR
	output [7:0] LEDR;
	// LEDR7-0: Q7-0

	shifter s0 (
		.LoadVal(SW[7:0]),
		.Load_n(KEY[1]),
		.ShiftRight(KEY[2]),
		.ASR(KEY[3]),
		.clk(KEY[0]),
		.reset_n(SW[9]),
		.Q(LEDR[7:0])
	);
endmodule

module shifter(LoadVal, Load_n, ShiftRight, ASR, clk, reset_n, Q); // 8-bit shift-register
	input [7:0] LoadVal;
	input Load_n, ShiftRight, ASR, clk, reset_n;
	output [7:0] Q;
	wire ext;

	signext se0(.sign(LoadVal[7]), .ASR(ASR), .extout(ext));

	shifterbit sb7(
		.load_val(LoadVal[7]), 
		.in(ext), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[7])
	);

	shifterbit sb6(
		.load_val(LoadVal[6]), 
		.in(Q[7]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[6])
	);

	shifterbit sb5(
		.load_val(LoadVal[5]), 
		.in(Q[6]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[5])
	);

	shifterbit sb4(
		.load_val(LoadVal[4]), 
		.in(Q[5]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[4])
	);

	shifterbit sb3(
		.load_val(LoadVal[3]), 
		.in(Q[4]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[3])
	);

	shifterbit sb2(
		.load_val(LoadVal[2]), 
		.in(Q[3]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[2])
	);

	shifterbit sb1(
		.load_val(LoadVal[1]), 
		.in(Q[2]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[1])
	);

	shifterbit sb0(
		.load_val(LoadVal[0]), 
		.in(Q[1]), 
		.shift(ShiftRight), 
		.load_n(Load_n), 
		.clk(clk), 
		.reset_n(reset_n), 
		.out(Q[0])
	);
endmodule

// a single-bit shift-register
module shifterbit(load_val, in, shift, load_n, clk, reset_n, out);
	input load_val, in, shift, load_n, clk, reset_n;
	output out;
	wire mux0to1, mux1toff0;

	mux mux0(.a(out), .b(in), .control(shift), .muxout(mux0to1));
	mux mux1(.a(load_val), .b(mux0to1), .control(load_n), .muxout(mux1toff0));
	dflipflop ff0(.d(mux1toff0), .q(out), .clk(clk), .reset_n(reset_n));
endmodule

// Sign-extension
module signext(sign, ASR, extout);
	input sign, ASR;
	output extout;
	reg extout;

	always @(*)
	begin
		if (ASR == 1'b0) // ASR 0 -> logic right shift
			extout = 1'b0;
		else            // ASR 1 -> arithmetic right shift
			extout = sign;
	end
endmodule

// a positive edge-triggered flip-flop
module dflipflop(d, q, clk, reset_n);
	input d, clk, reset_n;
	output q;
	reg q;

	always @(posedge clk) // triggered every time when clock rises
	begin
		if (reset_n == 1'b0) // when reset_n is 0, output 0
			q <= 0;
		else                 // when reset_n is 1, output 8-bit d
			q <= d;
	end
endmodule 

// a 2-to-1 multiplexer
module mux(a, b, control, muxout); // 0->a / 1->b
	input a, b, control;
	output muxout;
	reg muxout;

	always @(*)
	begin
		case(control)
			1'b0: muxout = a;
			1'b1: muxout = b;
		endcase
	end
endmodule
