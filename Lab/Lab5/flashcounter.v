module flashcountermain(SW, CLOCK_50, HEX0);
	input [8:0] SW;
	// [1:0] => speed
	// [2] => enable
	// [3] => par_load
	// [7:4] => data of par_load
	// [8] => reset_n
	input CLOCK_50;
	output [6:0] HEX0;
	wire [3:0] hex_in;

	flashcounter fc0(
		.clock(CLOCK_50),
		.enable(SW[2]),
		.par_load(SW[3]),
		.data(SW[7:4]),
		.reset_n(SW[8]),
		.speed(SW[1:0]),
		.out(hex_in)
		);

	hex h0(.in(hex_in), .out(HEX0));

endmodule

module flashcounter(clock, enable, par_load, data, reset_n, speed, out);
	input clock, enable, par_load, reset_n;
	input [1:0] speed;
	input [3:0] data;
	output [3:0] out;

	wire [27:0] rd1hz_out;
	wire [27:0] rd05hz_out;
	wire [27:0] rd025hz_out;
	reg dc_enable;

	ratedivider rd1hz(
		.clock(clock), 
		.enable(enable), 
		.data({2'b00, 26'd49999999}), 
		.reset_n(reset_n), 
		.out(rd1hz_out)
	);

	ratedivider rd05hz(
		.clock(clock), 
		.enable(enable), 
		.data({1'b0, 27'd99999999}), 
		.reset_n(reset_n), 
		.out(rd05hz_out)
	);

	ratedivider rd025hz(
		.clock(clock), 
		.enable(enable), 
		.data({28'd199999999}), 
		.reset_n(reset_n), 
		.out(rd025hz_out)
	);

	always @(*)
	begin
		case(speed)
			2'b00: dc_enable = enable;
			2'b01: dc_enable = (rd1hz_out == 4'b0000) ? 1 : 0;
			2'b10: dc_enable = (rd05hz_out == 4'b0000) ? 1 : 0;
			2'b11: dc_enable = (rd025hz_out == 4'b0000) ? 1 : 0;
		endcase
	end

	displaycounter dc0(
		.clock(clock),
		.enable(dc_enable),
		.par_load(par_load),
		.data(data),
		.reset_n(reset_n),
		.out(out)
	);

endmodule

module displaycounter(clock, enable, par_load, data, reset_n, out);
	input clock;
	input enable;
	input par_load;
	input [3:0] data;
	input reset_n;
	output [3:0] out;
	reg [3:0] out;

	always @(posedge clock)
	begin
		if (reset_n == 1'b0)
			out <= 0;
		else if (par_load == 1'b1)
			out <= data;
		else if (enable == 1'b1)
			out <= out + 1'b1;
		else if (enable == 1'b0)
			out <= out;
			// begin
			// 	if (out == 4'b1111)
			// 		out <= 0;
			// 	else
			// 		out <= out + 1'b1;
			// end
	end

endmodule

module ratedivider(clock, enable, data, reset_n, out);
	input clock, enable, reset_n;
	input [27:0] data;
	output [27:0] out;
	reg [27:0] out;

	always @(posedge clock)
	begin
		if (reset_n == 1'b0)
			out <= data; // parallel load
		else if (enable == 1'b1)
			begin
				if (out == 0)
					out <= data;
				else
					out <= out - 1'b1;
			end
	end

endmodule

module hex(in,out);
	input [3:0] in;
	output [6:0] out;
	
	zero m1(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[0])
		);
	one m2(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[1])
		);
	two m3(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[2])
		);
	three m4(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[3])
		);
   four m5(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[4])
		);
	five m6(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[5])
		);
	six m7(
		.a(in[0]),
		.b(in[1]),
		.c(in[2]),
		.d(in[3]),
		.m(out[6])
		);
endmodule

module zero(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((b & c) | (~a & d) | (~a & ~c) | (~b & ~c & d) | (a & c & ~d) | (b & ~c & ~d));
endmodule

module one(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((~c & ~a) | (~c & ~d) | (d & a & ~b) | (~d & a & b) | (~d & ~a & ~b));
endmodule

module two(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((c & ~d) | (~c & d) | (a & ~c) | (a & ~b) | (~d & ~a & ~b));
endmodule

module three(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((c & a & ~b) | (c & ~a & b) | (d & ~a & ~b) | (~c & ~a & ~b) | (~c & a & b) | (b & ~c & ~d));
endmodule

module four(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;

   assign m = ~((b & d) | (~a & d) | (~c & ~a) | (c & d & ~b) | (~d & ~a & b));
endmodule

module five(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((b & d) | (~a & d) | (~a & c) | (~a & ~b) | (~b & c & ~d) | (~b & ~c & d));
endmodule

module six(a,b,c,d,m);
	input a;
	input b;
	input c;
	input d;
	output m;
	
	assign m = ~((d & a) | (d & b) | (~c & b) | (~a & c & ~d) | (~b & c & ~d) | (~b & ~c & d));
endmodule

