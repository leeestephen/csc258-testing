vlib work

vlog -timescale 1ns/1ns 8bitcounter.v

vsim countermain

log {/*}

add wave {/*}

# force {enable} 1

# force {clock} 0 0, 1 10 -r 20

# force {clear_b} 1 0, 0 5, 1 10

# run 2000 ns

force {SW[0]} 0
force {SW[1]} 0
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 1
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 1
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 1
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 1
run 5ns

force {SW[0]} 0
force {SW[1]} 0
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 1
force {KEY[0]} 1
run 5ns

force {SW[0]} 1
force {SW[1]} 0
force {KEY[0]} 0
run 5ns

force {SW[0]} 1
force {SW[1]} 0
force {KEY[0]} 1
run 5ns
