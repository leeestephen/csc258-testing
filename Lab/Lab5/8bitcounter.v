module counter(KEY, SW, HEX0, HEX1);
	input [0:0] KEY;
	//Clock
	input [1:0] SW;
	//[1] = Enable
	//[0] = Clear_b
	output [0:6] HEX0, HEX1;
	wire c0, c1, c2, c3, c4, c5, c6, c7;
	
	my_tff t0(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(SW[1]),
		.q(c0)
	);
	
	my_tff t1(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(SW[1] & c0),
		.q(c1)
	);
	
	my_tff t2(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1),
		.q(c2)
	);
	
	my_tff t3(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1 & c2),
		.q(c3)
	);
	
	my_tff t4(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1 & c2 & c3),
		.q(c4)
	);
	
	my_tff t5(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1 & c2 & c3 & c4),
		.q(c5)
	);
	
	my_tff t6(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1 & c2 & c3 & c4 & c5),
		.q(c6)
	);
	
	my_tff t7(
		.clk(KEY[0]),
		.clear_b(SW[0]),
		.t(c0 & c1 & c2 & c3 & c4 & c5 & c6),
		.q(c7)
	);
	
	hex h0(
		.SW({c3, c2, c1, c0}),
		.HEX0(HEX0[0:6])
	);
	
	hex h1(
		.SW({c7, c6, c5, c4}),
		.HEX0(HEX1[0:6])
	);
	
endmodule

module my_tff(clk, clear_b, t, q);
	input t;
	input clk;
	input clear_b;
	output q;
	reg TFFout;
	
	always @(posedge clk, negedge clear_b)
	begin
		if (clear_b == 1'b0)
			TFFout <= 0;
		else if (t == 1'b0)
		 	TFFout <= q;
		else if (t == 1'b1)
			TFFout <= ~q;
//		else
//			TFFout <= t ^ q;
	end
	
	assign q = TFFout;
	
endmodule

module hex(SW, HEX0);
	input [9:0] SW;
	output [0:6] HEX0;
	
	hex_0 zero(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[0])
		);
		
	hex_1 one(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[1])
		);
		
	hex_2 two(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[2])
		);
		
	hex_3 three(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[3])
		);
		
	hex_4 four(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[4])
		);
		
	hex_5 five(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[5])
		);
		
	hex_6 six(
		.c3(SW[3]),
		.c2(SW[2]),
		.c1(SW[1]),
		.c0(SW[0]),
		.m(HEX0[6])
		);
		
endmodule

module hex_0(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & ~c2 & ~c1 & c0 | ~c3 & c2 & ~c1 & ~c0 | c3 & c2 & ~c1 & c0 | c3 & ~c2 & c1 & c0;
	
endmodule

module hex_1(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & c2 & ~c1 & c0 | c2 & c1 & ~c0 | c3 & c2 & ~c0 | c3 & c1 & c0;
	
endmodule

module hex_2(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //bianry digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & ~c2 & c1 & ~c0 | c3 & c2 & ~c0 | c3 & c2 & c1;
	
endmodule

module hex_3(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & ~c2 & ~c1 & c0 | ~c3 & c2 & ~c1 & ~c0 | c2 & c1 & c0 | c3 & ~c2 & c1 & ~c0;
	
endmodule

module hex_4(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & c0 | ~c3 & c2 & ~c1 | ~c2 & ~c1 & c0;
	
endmodule

module hex_5(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & ~c2 & c0 | ~c3 & ~c2 & c1 | ~c3 & c1 & c0 | c3 & c2 & ~c1 & c0;
	
endmodule

module hex_6(c3, c2, c1, c0, m);
	input c3; //binary digit 4
	input c2; //binary digit 3
	input c1; //binary digit 2
	input c0; //binary digit 1
	output m; //output
	
	assign m = ~c3 & ~c2 & ~c1 | ~c3 & c2 & c1 & c0 | c3 & c2 & ~c1 & ~c0;
	
endmodule
