vlib work

vlog -timescale 1ns/1ps flashcounter.v

vsim flashcounter

log {/*}

add wave {/*}



force {enable} 1

force {clock} 0 0ps, 1 1ps -r 2ps

force {data} 2#0000

force {par_load} 0

force {reset_n} 0 0ps, 1 3ps

force {speed} 2#01

run 1000000ns


#force {SW[0]} 0 0, 1 100 -repeat 200
#force {SW[1]} 0 0, 1 200
#force {SW[2]} 0 0, 1 20
#force {SW[3]} 1
#force {CLOCK_50} 0 0, 1 1 -repeat 2
#run 400ns
