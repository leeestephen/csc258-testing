# Goal
- create and use on-chip Block Random Access Memories
- use Video Graphics Adapter

# Part I: Memory unit
- Creating a mini-RAM unit.
- Make use of the IP Catalog built into Quartus.
	- Follow lab instructions to create a 4-bit RAM unit with 32 words.
- Once created, connect this RAM to the switches, keys and HEX.
- Read & Write Timing
	- Read:
		- Note slight delay after clock signal, before data appears.
	- Write:
		- Note that only D1 and D2 are written (because of the WriteEn signal).

# Part II: VGA display
- Specifying the inputs to the VGA adaptor will set a single pixel to a single colour.
	- How would you make a box on the screen?
- Given input coordinates X and Y, make a 4x4 box of coloured pixels, using X and Y as the top left corner of the box.
- Components needed:
	- VGAadaptor(providedbyus)
	- Datapaththattakesin:
	- X and Y (through switches)
	- control signals (from KEYs, clock and FSM)
	- FSM:
	- Controls datapath to load X and Y values, and iterate through the pixel locations that need to be updated (relative to X and Y).
- Hints:
	- Have tests to verify that each component works on its own.
	- Try using the VGA adaptor to draw a single pixel, make sure the datapath works on its own, verify that the FSM is moving from state to state as expected.
	- Consider using counters to store the offsets from X and Y that need to be displayed.
	- Background is black by default, so test with pixel colour values other than (0,0,0)

# Part III: Animation

