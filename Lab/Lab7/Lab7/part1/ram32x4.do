vlib work

vlog -timescale 1ps/1ps ram32x4.v

vsim -L altera_mf_ver ram32x4

log {/*}

add wave {/*}

force {clock} 0 0, 1 20 -r 40

force {address} 2#00001 0, 2#00011 30, 2#00001 70, 2#00011 110, 2#00001 150

force {data} 2#1010 0, 2#1111 30, 2#0000 70

force {wren} 0 0, 1 5, 0 30, 1 50, 0 70


run 400ps