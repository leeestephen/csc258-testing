[TOC]

# Handout

[project_proposal.pdf](./CSC258_Fall2017_ProjectProposal.pdf) (Contains a link to your project proposal form on the Google Forms (here is the link as well: [Project Proposal Submission](https://goo.gl/forms/VGOOwBGXLnyrIwsI3)). The deadline for your project proposal is **Friday, November 10th at 11:59pm EST**, regardless of what day your lab is on.

### Potentially Useful Resources:

NOTE: If you use any code that you did not write in your project, you need to

(a) acknowledge it in advance and 

(b) ensure that this is not on its own a significant component of your project (i.e., the code YOU write should be the equivalent of 3-weeks worth of labs).

-   Keyboard (linked from the University of Washington):
    -   Handout: <https://class.ee.washington.edu/271/hauck2/de1/keyboard/PS2Keyboard.pdf>
    -   Design Files: <https://class.ee.washington.edu/271/hauck2/de1/keyboard/KeyboardFiles.zip>
-   Other keyboard resources:
    -   John Loomis website: <http://www.johnloomis.org/digitallab/ps2lab1/ps2lab1.html>
    -   Daniel Chan's [keyboard files](https://portal.utoronto.ca/bbcswebdav/pid-6273225-dt-content-rid-40497928_2/xid-40497928_2)
-   Audio (Altera’s Lab on Basic Digital Signal Processing): 
    -   Handout: <ftp://ftp.altera.com/up/pub/Altera_Material/Laboratory_Exercises/Digital_Logic/DE1-SoC/verilog/lab12_Verilog.pdf>
    -   Design Files: [ftp://ftp.altera.com/up/pub/Altera_Material/Laboratory_Exercises/Digital_Logic/DE1-SoC/verilog/lab12_design_files.zip](ftp://ftp.altera.com/up/pub/Altera_Material/Laboratory_Exercises/Digital_Logic/DE1-SoC/verilog/lab12_design_files.zip)
-   Random Numbers:
    -   Look at [Linear Feedback Shift Registers](https://en.wikipedia.org/wiki/Linear-feedback_shift_register).


-   Additional advice on various aspects of your project design, including link to program for converting BMP files into MIF, tips for using VGA display, generating random numbers, and general design and debugging tips.
    -   [project_advice.pdf](https://portal.utoronto.ca/bbcswebdav/pid-6273225-dt-content-rid-39628505_2/xid-39628505_2)

### Marking for the project

For your project, we will be marking you on whether you meet your personally defined milestones, as
well as course-level goals. The course-level goals for the project are:

1. For you to demonstrate your understanding of digital logic, hardware, and Verilog.
2. For you to implement a creative application of these topics.

---

# SkiClub

![img](./alpine_ski.png)

### Team

-   Junkai Li, 1002116578, junkai.li@mail.utoronto.ca
-   Your name, student# and email here

### Project Info

-   **Description:**

    SkiClub is a skiing arcade game. Players plays as a skier, who can move left, right, increase forward speed or decrease forward speed. The goal for the player is to control a skier through a downhill piste as long as possible, avoid obstacles (trees, ice, and moving animals), and collect ski gears in the shortest time possible. Ski gears can be collected when the skier has contact with it. The player will use keyboard (W, S, A, D) to control the skier. As the game goes on, the speed of the forward speed will increase. In every new game, the skier has 3 health. If the skier hits the obstacles, he loses 1 health. If the skier collects the gear, he gains 1 health. When he loses all 3 health, the game will show game over message and stop running until the player hit R to reset the game.

-   **How does this project relate to material in class?**

    We will use hierarchical design since we need a better organization for this more complicated project and we also need to create many obstacles that share the same property. We will use rate dividers to control the forward speed of the skier (i.e. how fast obstacles and gears appear). We will use registers counters to draw objects and also keep track of health of the skier, the distance the skier has gone and the time of the skier has used. We will use finite state machines to control the state (killed or not) and the movement of the skier The appearance of the obstacles and gears is also controlled by finite state machine. We will use random generators to generate obstacles and gears. 

-   **Cool things about this project:**

    For 258 students, the cool thing is that we combine all we have learnt and apply it in a creative way, and also that we use some that haven't been taught in class. SkiClub is based on a popular arcade game called Alpine Ski developed by Taito in 1981. So for non-258 students, this game will provide an enjoyable experience.

-   **Why does the idea of working on this appeal to you personally?**

    Working on this gives me an opportunity to make use of all we have learnt and create something more practical instead of the abstract modules. Also, 80's retro arcade game is always appealing to me to have a simple yet addictive gameplay.

## Milestones

### Week 1

-   create skier control system
-   create random generators

### Week 2

-   work on the aesthetic of the objects
-   create obstacle control system

### Week 3

-   create gear control system
-   finalize the game