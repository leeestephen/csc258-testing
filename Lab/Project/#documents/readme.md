# SkiClub

## Milestones
- Week 1
    -  create skier control system
        - After the player presses `KEY[0]`, the game starts.
        - When the player press `KEY[1]`, the skier move up.
        - When the player press `KEY[2]`, the skier move down.
    -  create random generators (module)
- Week 2
    -  work on the aesthetic of the objects
    -  create obstacle control system
- Week 3
    -  create gear control system
    -  finalize the games

## Intro
SkiClub is based on a arcade game called Alpine Ski. 
- For our minimum deliveralbe, we created a basic single player obstacle-dodging game and hopefully, we can improve the aesthetic. 
- In addition, if it's possible, we wanted to add speed control into the game. 
- For our maximum deliverable, we wanted to have multiple levels with increasing difficulty.

The current rules of the game are the following:
- After the player presses `R` or `KEY[0]`, the game starts.
- When the player press `D` or `KEY[1]`, the skier move right.
- When the player press `A` or `KEY[2]`, the skier move left.
- When the skier collects 1 gear, the score increments 1 point.
- When the skier hits 1 obstacle, the game stops and displays that the number of lives subtracts 1.
- When the number of lives reaches 0, the game is ended and displays the game over text, scores, and remaining lives.
- When the skier passes the finish line, the game is ended and displays the finish text, scores, and remaining lives.

Possible updates for the rules (ignore it for now):
- Original speed is ? per second.
- When the game starts, timer also starts.
- When the player press `W` or `KEY[3]`, the skier move 2 times faster.
- When the player press `S` or `KEY[4]`, the skier move 1/2 times slower, but must greater than or equal to the original speed.
- When the skier travels 1 second, the speed increments ? unit.
- When the skier passes the finish line, , the game is ended and displays the finish text, scores, remaining lives, and also time it used.
- When the player hits 1 obstacle, the speed changes back to original and displays that the number of lives subtracts 1.

## Top-level schematic and I/O declearation
- **Inputs:**
    - `R` or `KEY[0]`: game starts/restarts
    - `D` or `KEY[1]`: move right if there's room
    - `A` or `KEY[2]`: move left if there's room
- **Outputs:**
    - `hsync`: horizontal sync
    - `vsync`: vertical sync
    - `RGB`: to VGA port
- **Modules:**
    - `score_counter`: 2 digit counter that counts from 00 to 99 for score
    - `live_counter`: 2 digit counter that counts from 00 to 99 for lives
    - `ski_control`: top-level control 
    - `ski_graph`: for graphics
    - `ski-text`: for text display; stores all necessary symbols

## Game logic and graphic generation
Since our implementation uses an object-mapped pixel generation circuit, we need to keep track of objects’ positions in the screen. 

For every of the 3 objects represented (left paddle, right paddle and ball), two variables are used: current position and next position. On the clock rise, the next position data is transferred to the current position variable and the graphics generation module outputs the correct vga signals.

```verilog
always @(posedge clk, posedge reset)
      if (reset)
         begin
            barr_y_reg <= 0;
            barl_y_reg <= 0;
            ball_x_reg <= 0;
            ball_y_reg <= 0;
            x_delta_reg <= 10'h004;
            y_delta_reg <= 10'h004;
         end   
      else
         begin
            barr_y_reg <= barr_y_next;
            barl_y_reg <= barl_y_next;
            ball_x_reg <= ball_x_next;
            ball_y_reg <= ball_y_next;
            x_delta_reg <= x_delta_next;
            y_delta_reg <= y_delta_next;
         end
```

## Animation
In order to create the animated display result, registers are used to store the boundaries of the ball. The values of these registers are updated every time the screen of the VGA monitor refreshes. We need to determine how to change these values to make the game flow smooth and natural.

We start with the ball moving at a constant speed. The direction of the ball changes when it hits the paddles, the bottom or top of the screen. We decompose the velocity into an x-component and a y-component, whose value can either be 2 or -2. The bounce motion of the ball is simulated by- when the ball hits the paddles, the x-component of the velocity flips its sign and the y-component stays the same; when the ball hits the top or the bottom of the screen, the y-component of the velocity flips its sign and the x-component does not change.

This method works well except there is one drawback: because the ball always moves at 45 degrees angle, there is a repetitive pattern of where it hits the paddle every time it bounces. As it comes to be obvious to the players, the game becomes boring very fast. To make our game more unpredictable, therefore, more interesting, we implement a new method for the ball to bounce off the paddles. We are going to use the right paddle as an example. The paddle is divided into five regions as shown in Figure.1.

Figure 1

When the ball hits the paddle, the y-component stays the same, but the x-component not only change its sign, but also change its magnitude depending on the impact region of the. For example, regardless of the velocity of the ball when it hits the paddle, as long as it hits at the outmost parts of the paddle, the x-component of the bounce speed becomes -4.