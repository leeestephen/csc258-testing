// Part 2 skeleton

module part2
	(
		CLOCK_50,						//	On Board 50 MHz
		// Your inputs and outputs here
        KEY,
		// The ports below are for the VGA output.  Do not change.
		VGA_CLK,   						//	VGA Clock
		VGA_HS,							//	VGA H_SYNC
		VGA_VS,							//	VGA V_SYNC
		VGA_BLANK_N,						//	VGA BLANK
		VGA_SYNC_N,						//	VGA SYNC
		VGA_R,   						//	VGA Red[9:0]
		VGA_G,	 						//	VGA Green[9:0]
		VGA_B   						//	VGA Blue[9:0]
	);

	input			CLOCK_50;				//	50 MHz
	input   [3:0]   KEY;

	// Declare your inputs and outputs here
	// Do not change the following outputs
	output			VGA_CLK;   				//	VGA Clock
	output			VGA_HS;					//	VGA H_SYNC
	output			VGA_VS;					//	VGA V_SYNC
	output			VGA_BLANK_N;				//	VGA BLANK
	output			VGA_SYNC_N;				//	VGA SYNC
	output	[9:0]	VGA_R;   				//	VGA Red[9:0]
	output	[9:0]	VGA_G;	 				//	VGA Green[9:0]
	output	[9:0]	VGA_B;   				//	VGA Blue[9:0]
	
	wire resetn;
	assign resetn = KEY[0];
	
	// Create the colour, x, y and writeEn wires that are inputs to the controller.
	wire [2:0] colour;
	wire [7:0] x;
	wire [6:0] y;
	wire writeEn;
	wire [1:0] select;
	wire go;

	// Create an Instance of a VGA controller - there can be only one!
	// Define the number of colours as well as the initial background
	// image file (.MIF) for the controller.
	vga_adapter VGA(
			.resetn(resetn),
			.clock(CLOCK_50),
			.colour(colour),
			.x(x),
			.y(y),
			.plot(writeEn),
			/* Signals for the DAC to drive the monitor. */
			.VGA_R(VGA_R),
			.VGA_G(VGA_G),
			.VGA_B(VGA_B),
			.VGA_HS(VGA_HS),
			.VGA_VS(VGA_VS),
			.VGA_BLANK(VGA_BLANK_N),
			.VGA_SYNC(VGA_SYNC_N),
			.VGA_CLK(VGA_CLK));
		defparam VGA.RESOLUTION = "160x120";
		defparam VGA.MONOCHROME = "FALSE";
		defparam VGA.BITS_PER_COLOUR_CHANNEL = 1;
		defparam VGA.BACKGROUND_IMAGE = "black.mif";
			
	// Put your code here. Your code should produce signals x,y,colour and writeEn/plot
	// for the VGA controller, in addition to any other functionality your design may require.
	
    
   // Instansiate datapath
	datapath d0(
		.clock(CLOCK_50),
		.up(KEY[1]),
		.down(KEY[2]),
		.select(select),
		.go(go),
		.x_out(x),
		.y_out(y),
		.c_out(colour)
	);

   // Instansiate FSM control
   control c0(
		.clock(CLOCK_50),
		.go(go),
		.reset_n(resetn),
		.writeEn(writeEn),
		.select(select)
	);
    
endmodule


module datapath(clock, up, down, select, x_out, y_out, c_out);

	input clock;
	input up;
	input down;
	input [1:0] select;
	output [7:0] x_out;
	output [6:0] y_out;
	output reg [2:0] c_out;
	
	reg [7:0] xr = 0;
	reg [6:0] yr = 56;
	reg count_enable;
	reg start_enable = 0;
	reg reset_enable = 0;
	reg [3:0] count;
	reg [26:0] rate;
	
	always @(posedge clock)
	begin
		if (select == 2)
			rate <= 0;
		else
		begin
			if (rate == 0)
				rate <= 24999999;
			else
				rate <= rate - 1;
		end
	end
	
	always @(posedge clock)
	begin
		if (go)
			go <= 0;
		if (select == 2)
		begin
			if (!reset_enable)
			begin
				c_out <= 0;
				xr <= 0;
				yr <= 0;
				reset_enable = 1;
			end
			if (xr < 160 && reset_enable)
			begin
				xr <= xr + 1;
			end
			if (yr < 120 && reset_enable)
			begin
				yr <= yr + 1;
			end
			else if (xr == 160 && yr == 120 && reset_enable)
			begin
				go <= 1;
				reset_enable <= 0;
			end
		end
		else if (select == 0)
		begin
			go <= 1;
		end
		else if (select == 1)
		begin
			if (!start_enable)
			begin
				c_out <= 7;
				count <= 0;
				xr <= 0;
				yr <= 0;
				count_enable <= 1;
				start_enable <= 1;
			end
			if (count_enable && start_enable)
			begin
				if (count == 15)
				begin
					count_enable <= 0;
					count <= 0;
				end
				else
					count <= count + 1;
			end
			if (xr == 160 && start_enable)
			begin
				go <= 1;
				start_enable <= 0;
			end
			else if (rate == 0 && start_enable)
			begin
				xr <= xr + 1;
				if (!up)
				begin
					yr <= yr + 1;
				end
				if (!down)
				begin
					yr <= yr - 1;
				end
				count_enable == 1;
			end
		end
	end
	
	assign x_out = xr + count[1:0];
	assign y_out = yr + count[3:2];
	
endmodule

module control(clock, go, reset_n, writeEn, select);

	input clock;
	input go;
	input reset_n;
	
	output reg writeEn = 1;
	output reg [1:0] select;
	
	reg [2:0] current_state, next_state;
	
	localparam 	S_OBSTACLE = 0,
					S_OBSTACLE_WAIT = 1,
					S_START = 2,
					S_START_WAIT = 3,
					S_RESET = 4,
					S_RESET_WAIT = 5;
					
	always @(*)
	begin
		case(current_state)
			S_OBSTACLE: next_state = go ? S_OBSTACLE_WAIT : S_OBSTACLE;
			S_OBSTACLE_WAIT: next_state = go ? S_START : S_OBSTACLE_WAIT;
			S_START: next_state = go ? S_START : S_START_WAIT;
			S_START_WAIT: next_state = go ? S_RESET : S_START_WAIT;
			S_RESET: next_state = go ? S_RESET : S_RESET_WAIT;
			S_RESET_WAIT: next_state = go ? S_OBSTACLE : S_RESET_WAIT;
		default: next_state = S_RESET;
	end
	
	always @(*)
	begin
		case(current_state)
			S_OBSTACLE: select = 0;
			S_START: select = 1;
			S_RESET: select = 2;
	end
	
	always@(posedge clock)
    begin
        if(!resetn)
            current_state <= S_RESET;
        else
            current_state <= next_state;
    end
	 
endmodule

