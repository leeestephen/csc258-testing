module obstacle_top(
	CLOCK_50, 				// On Board 50 MHz
	KEY, SW, 				// Use KEY and SW for now
	VGA_CLK,				// VGA Clock
	VGA_HS,					// VGA H_SYNC
	VGA_VS,					// VGA V_SYNC
	VGA_BLANK_N,			// VGA BLANK
	VGA_SYNC_N,				// VGA SYNC
	VGA_R, 					// VGA 10-bit Red[9:0]
	VGA_G,					// VGA 10-bit Green[9:0]
	VGA_B 					// VGA 10-bit Blue[9:0]
);
	input CLOCK_50;			// 50 MHz clock
	input [9:0] SW;			// No use for now
	input [4:0] KEY;
	// DO NOT change the following outputs
	output VGA_CLK;   		// VGA Clock      
	output VGA_HS;			// VGA H_SYNC
	output VGA_VS;			// VGA V_SYNC
	output VGA_BLANK_N;		// VGA BLANK
	output VGA_SYNC_N;		// VGA SYNC
	output [9:0] VGA_R; 	// VGA 10-bit Red[9:0]
	output [9:0] VGA_G;	 	// VGA 10-bit Green[9:0]
	output [9:0] VGA_B;		// VGA 10-bit Blue[9:0]
	
	wire resetn;
	assign resetn = KEY[0];
	
	// wire up;
	// assign up = KEY[1];

	// wire down;
	// assign down = KEY[2];
	
	// inputs to the VGA adapter, from datapath.
	wire [2:0] colour_out;
	wire [8:0] x_out;
	wire [7:0] y_out;
	// wire writeEn;
	// wire ld_colour;

	obstacle obstacle0(CLOCK_50, resetn, x_out, y_out, colour_out);
		//, colour_in, draw, ld_colour, up, down);

	// fsm game_fsm(CLOCK_50, resetn, enable, ld_colour, writeEn);

	vga_adapter VGA(.resetn(resetn), 
		.clock(CLOCK_50), .colour(colour_out),
		.x(x_out), .y(y_out), .plot(writeEn),
		/* Signals for the DAC to drive the monitor. */
		.VGA_R(VGA_R), .VGA_G(VGA_G), .VGA_B(VGA_B),
		.VGA_HS(VGA_HS), .VGA_VS(VGA_VS),
		.VGA_BLANK(VGA_BLANK_N), .VGA_SYNC(VGA_SYNC_N),
		.VGA_CLK(VGA_CLK)
	);
		defparam VGA.RESOLUTION = "160x120";
		defparam VGA.MONOCHROME = "FALSE";
		defparam VGA.BITS_PER_COLOUR_CHANNEL = 1;//3-bit RGB
		defparam VGA.BACKGROUND_IMAGE = "black.mif";
endmodule