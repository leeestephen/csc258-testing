//========================================
// Random
//========================================

module random(clock,reset_n,q);	// 4-bit Pseudo Random Number
		input clock,reset_n;    // when resets, seed number is
		output [3:0] q;			// 4'b0111 = 4'd7
		
		dff_0 m1(clock,reset_n,q[2] ^ q[3],q[0]); // XOR
		dff_1 m2(clock,reset_n,q[0],q[1]);
		dff_1 m3(clock,reset_n,q[1],q[2]);
		dff_1 m4(clock,reset_n,q[2],q[3]);
endmodule

module dff_0(clock,reset_n,data_in,q); // d flip flop that
		input clock,reset_n,data_in;   // outputs 0 when resets
		output reg q;
		always@(posedge clock)
		   begin
			if(reset_n == 0) 
				  q <= 1'b0; 
			else
				  q<= data_in;
			end
endmodule

module dff_1(clock,reset_n,data_in,q); // d flip flop that
		input clock,reset_n,data_in;   // outputs 1 when resets
		output reg q;
		always@(posedge clock)
		   begin
			if(reset_n == 0) 
				  q <= 1'b1; 
			else
				  q<= data_in;
			end
endmodule
