module clk_reduce(clk, reset_n, clk_100Hz);
input clk, reset_n;
output clk_100Hz;

reg clk_1Hz = 1'b0;
reg [27:0] counter;

always@(negedge reset_n or posedge clk)
begin
    if (!reset_n)
        begin
            clk_1Hz <= 0;
            counter <= 0;
        end
    else
        begin
            counter <= counter + 1;
            if ( counter == 499999) 
                begin
                    counter <= 0;
                    clk_100Hz <= ~clk_100Hz;
                end
        end
end
endmodule   