//========================================
// Graphics / top datapath
//========================================

module graphics(clk, reset_n, enable, draw, 
	ld_colour, up, down, 
	x_out, y_out, colour_out);

	input clk, reset_n, enable, draw;
	input ld_colour;
	input up, down;

	output [8:0] x_out;		 // 9-bit x axis pixel
	output [7:0] y_out;		 // 8-bit y axis pixel
	output [2:0] colour_out; // 3-bit colour output
	
	reg [8:0] x;
	wire [7:0] y;
	reg [2:0] colour;
	
	// could vary according to shape
	wire [1:0] y_count;
	// could vary according to shape
	wire [1:0] x_count;
	// where y starts
	reg [7:0] y_start;
	
	// when rate division done, frame enabled
	wire [20:0] rate_out;
	wire frame_enable;
	// when frame division done, x enabled 
	wire [3:0] frame_out;
	wire x_enable;
	// used for enabling y to count when x is done
	wire y_enable;
	
	// register for x, y_start, colour
	always @(posedge clk) 
	begin
		if (!reset_n) 
		begin
			x <= 9'b0;
			y_start <= 8'b0;
			colour <= 3'b0;
		end
		else 
		begin
			x <= 9'd10;
			y_start <= 8'd60;
			if (ld_colour) 
			begin
				if (!draw) 
				begin
					colour <= 3'b000; // black
				end
				else 
				begin
					colour <= 3'b111; // white
				end
			end
		end
	end
	
	// rate divider
	rate_divider rate(clk, reset_n, enable, rate_out); 
	assign frame_enable = (rate_out == 20'd0) ? 1 : 0;
	
	// frame counter
	frame_counter frame(clk, frame_enable, reset_n, frame_out);
	assign x_enable = (frame_out == 4'd10) ? 1 : 0;
	
	// x counter for square 4 * 4 skier
	// threshold = 2'b11
	// TODO: change x and y counter to shape
	x_counter cx(clk, enable, reset_n, x_count);
	
	// assign y_enable = 1 when x goes through 1 row
	assign y_enable = (x_count == 2'b11) ? 1 : 0;
	
	// y counter for square 4 * 4 skier
	// TODO: change x and y counter to shape
	y_counter cy(clk, y_enable, reset_n, y_count);

	y_movement_counter y_move(clk, enable, reset_n, up, down, y);
	
	// outputs
	assign x_out = x + x_count;
	assign y_out = y_start + y + y_count;
	assign colour_out = colour;
endmodule

// x counter, count to the threshold to change row and shape
module x_counter(clk, enable, reset_n, out);
	input clk, enable, reset_n;
	// could vary according to shape
	output reg [1:0] out;
	
	always @(posedge clk) 
	begin
		if (!reset_n)
			out <= 2'b0;
		else if (enable) 
		begin
			if (out == 2'b11)
				out <= 2'b0;
			else
				out <= out + 1'b1;
		end
	end
endmodule

// y counter, count to the threshold to change column and shape
module y_counter(clk, enable, reset_n, out);
	input clk, enable, reset_n;
	// could vary according to shape
	output reg [1:0] out;
	
	always @(posedge clk) 
	begin
		if (!reset_n)
			out <= 2'b0;
		else if (enable) 
		begin
			if (out == 2'b11)
				out <= 2'b0;
			else
				out <= out + 1'b1;
		end
	end
endmodule

// y movement counter, count up when up signal, down when down signal
module y_movement_counter(clk, enable, reset_n, up, down, q);
	input clk, enable, reset_n, up, down;
	output reg [7:0] q;

	always @(posedge clk) 
	begin
		if (reset_n) 
		begin
			q <= 8'd60;
		end
		else if (enable) 
		begin
			if (up && ~down) 
			begin
				if (q == 8'd0) 
				begin
					q <= q;
				end
				else 
				begin
					q <= q - 1'b1;
				end
			end
			else if (down && ~up) 
			begin
				if (q == 8'd114) 
				begin
					q <= q;
				end
				else 
				begin
					q <= q + 1'b1;
				end
			end
			else 
			begin
				q <= q;
			end
		end
	end
endmodule

// frame counter, count to 15 for every move 
// so that the frame could refresh
module frame_counter(clk, enable, reset_n, out);
	input clk, enable, reset_n;
	output reg [3:0] out;
	
	always @(posedge clk) 
	begin
		if (!reset_n)
			out <= 4'b0;
		else if (enable) 
		begin
			if (out == 4'b1111)
				out <= 4'b0;
			else
				out <= out + 1'b1;
		end
	end
endmodule

// rate divider
module rate_divider(clk, reset_n, enable, out);
		input clk;
		input reset_n;
		input enable;
		output reg [19:0] out;
		
		always @(posedge clk)
		begin
			if (!reset_n)
				out <= 20'd0;
			else if (enable) 
			begin
			   if (out == 20'd0)
					out <= 20'd1666666;
				else
					out <= out - 1'b1;
			end
		end
endmodule
