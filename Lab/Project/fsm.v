//========================================
// Game FSM control
//========================================

module fsm(clk, reset_n, enable, draw, ld_colour, writeEn);
	input clk, reset_n;
	output reg enable, draw, ld_colour, writeEn;

	reg [1:0] current_state, next_state;

	localparam [1:0] // 3 states (2-bit)
		LOAD = 2'b00, 
		LOAD_WAIT = 2'b01, 
		DRAW = 2'b10;

	always @(*) 
	begin
		case (current_state)
			LOAD: next_state = (!reset_n) ? LOAD_WAIT : LOAD;
			LOAD_WAIT: next_state = (!reset_n) ? LOAD_WAIT : DRAW;
			DRAW: next_state = DRAW;
		endcase
	end

	always @(*) 
	begin
		enable = 1'b0;
		ld_colour = 1'b0;
		draw = 1'b0;
		writeEn = 1'b0;
		case (current_state)
			DRAW: 
			begin
				enable = 1'b1;
				draw = 1'b1;
				ld_colour = 1'b1;
				writeEn = 1'b1;
			end
		endcase
	end

	always @(posedge clk) begin
		if (!reset_n) begin
			current_state <= LOAD;
		end
		else begin
			current_state <= next_state;
		end
	end
endmodule
