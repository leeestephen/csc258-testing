module obstacle(clock, resetn, x_out, y_out, colour_out);
	input clock, resetn;
	output [7:0] x_out, y_out;
	output [2:0] colour_out;

	reg [7:0] x_reg = 0;
	reg [7:0] x_reg = 0;
	reg [2:0] colour_reg = 3'b101;

	wire enable;
	wire [3:0] x_c = 0;
	wire [6:0] y_c = 0;
	wire y_begin = 0;

	always @(posedge clock or negedge resetn) 
	begin
		if (!resetn) 
		begin
			x_reg <= 7'b0;
			y_reg <= 7'b0;
			colour_reg <= 3'b101;
		end
		else 
		begin
			x_reg <= 7'd80;
			y_reg <= 7'd60;
			colour_reg <= 3'b1;
		end
	end

	clk_reduce slow_clk(clock, resetn, enable);
	x_counter xc(clock, resetn, enable, x_c);
	assign y_begin = (x_c == 5) ? 1 : 0; // x counts up to 4'd10px then begin to count y
	y_counter yc(clock, resetn, y_begin, y_c);

	assign x_out = x_reg + x_c;
	assign y_out = y_reg + y_c;
	assign colour_out = colour_reg;
endmodule


module x_counter(clock, resetn, enable, out);
	input clock, resetn, enable;
	output reg [3:0] out;

	always @(posedge clock or negedge resetn) 
	begin
		if (!resetn) 
		begin
			out <= 4'b0;
		end
		else if (enable) 
		begin
			if (out == 4'd5)
				out <= 4'b0;
			else 
			begin
				out <= out + 1;
			end
		end
	end
endmodule


module y_counter(clock, resetn, enable, out);
	input clock, resetn, enable;
	output reg [6:0] out;

	reg [6:0] limit;
	wire [3:0] random_out;

	random r0(clock, resetn, random_out);
	always @(*)
	begin
		if( random_out < 4'd6 )
			limit = 7'd30;
		else if ( random_out < 4'd11 )
			limit = 7'd60;
		else if ( random_out < 4'd15 )
			limit = 7'd90;
	end

	// y counter up to `limit`
	always @(posedge clock or negedge resetn) 
	begin
		if (!resetn) 
		begin
			out <= 7'b0;
		end
		else if (enable) 
		begin
			if (out == limit) 
			begin
				out <= 7'b0;
			end
			else 
			begin
				out <= out + 1; 
			end
		end
	end

endmodule

module random(clock,reset_n,q);	// 4-bit Pseudo Random Number
		input clock,reset_n;    // when resets, seed number is
		output [3:0] q;			// 4'b0111 = 4'd7
		
		dff_0 m1(clock,reset_n,q[2] ^ q[3],q[0]); // XOR
		dff_1 m2(clock,reset_n,q[0],q[1]);
		dff_1 m3(clock,reset_n,q[1],q[2]);
		dff_1 m4(clock,reset_n,q[2],q[3]);
endmodule

module dff_0(clock,reset_n,data_in,q); // d flip flop that
		input clock,reset_n,data_in;   // outputs 0 when resets
		output reg q;
		always@(posedge clock)
		   begin
			if(reset_n == 0) 
				  q <= 1'b0; 
			else
				  q<= data_in;
			end
endmodule

module dff_1(clock,reset_n,data_in,q); // d flip flop that
		input clock,reset_n,data_in;   // outputs 1 when resets
		output reg q;
		always@(posedge clock)
		   begin
			if(reset_n == 0) 
				  q <= 1'b1; 
			else
				  q<= data_in;
			end
endmodule


module clk_reduce(clk, reset_n, clk_100Hz);
input clk, reset_n;
output clk_100Hz;

reg clk_1Hz = 1'b0;
reg [27:0] counter;

always@(negedge reset_n or posedge clk)
begin
    if (!reset_n)
        begin
            clk_1Hz <= 0;
            counter <= 0;
        end
    else
        begin
            counter <= counter + 1;
            if ( counter == 499999) 
                begin
                    counter <= 0;
                    clk_100Hz <= ~clk_100Hz;
                end
        end
end
endmodule
