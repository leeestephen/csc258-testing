//SW[3:0] data inputs
//SW[0] u
//SW[1] v
//SW[2] w
//SW[3] x
//SW[9:8] select signal
//SW[8] s0
//SW[9] s1

//LEDR[0] output display

module mux4to1(LEDR, SW);
    input [9:0] SW;
    output [0] LEDR;

    wire m1, m2;

    mux2to1 u0(
        .x(SW[0]),
        .y(SW[2]),
        .s(SW[8]),
        .m(m1)
        );

    mux2to1 u1(
        .x(SW[1]),
        .y(SW[3]),
        .s(SW[8]),
        .m(m2)
        );

    mux2to1 u2(
        .x(m1),
        .y(m2),
        .s(SW[9]),
        .m(LEDR[0])
        );

endmodule

module mux2to1(x, y, s, m);
    input x; //selected when s is 0
    input y; //selected when s is 1
    input s; //select signal
    output m; //output
  
    assign m = s & y | ~s & x;
    // OR
    // assign m = s ? y : x;

endmodule
