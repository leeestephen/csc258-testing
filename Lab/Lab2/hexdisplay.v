//SW[3:0] data inputs
//SW[0] c0
//SW[1] c1
//SW[2] c2
//SW[3] c3

//HEX0[0] output hexdecimal display

module hexdisplay(HEX0, SW);
    input [3:0] SW;
    output [6:0] HEX0;

    seg_0 s0(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out0(HEX0[0])
    );
    
    seg_1 s1(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out1(HEX0[1])
    );
    
    seg_2 s2(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out2(HEX0[2])
    );
    
    seg_3 s3(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out3(HEX0[3])
    );
    
    seg_4 s4(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out4(HEX0[4])
    );
    
    seg_5 s5(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out5(HEX0[5])
    );
    
    seg_6 s6(
        .c0(SW[0]),
        .c1(SW[1]),
        .c2(SW[2]),
        .c3(SW[3]),
        .out6(HEX0[6])
    );
    
endmodule

// Segment 0 of hexdecimal display module
// Activate for: 0, 2, 3, 5, 7, 8, 9, A, C, E, F
module seg_0(c0, c1, c2, c3, out0);
    input c0;
    input c1;
    input c2;
    input c3;
    output out0;

    assign out0 = (~c3 & c2 & ~c1 & ~c0) | (~c3 & ~c2 & ~c1 & c0) | (c3 & c2 & ~c1 & c0) | (c3 & ~c2 & c1 & c0);
endmodule

// Segment 1 of hexdecimal display module
// Activate for: 0, 2, 3, 4, 7, 8, 9, A, d
module seg_1(c0, c1, c2, c3, out1);
    input c0;
    input c1;
    input c2;
    input c3;
    output out1;

    assign out1 = (c3 & c2 & ~c0)  |  (~c3 & c2 & ~c1 & c0)  |  (c2 & c1 & ~c0)  | (c3 & c1 & c0);
endmodule

// Segment 2 of hexdecimal display module
// Activate for: 0, 3, 4, 5, 6, 7, 8, 9, A, b, d
module seg_2(c0, c1, c2, c3, out2);
    input c0;
    input c1;
    input c2;
    input c3;
    output out2;

    assign out2 = (c3 & c2 & ~c0)  | (c3 & c2 & c1)  | (~c3 & ~c2 & c1 & ~c0);
endmodule

// Segment 3 of hexdecimal display module
// Activate for: 0, 2, 3, 5, 6, 8, 9, b, C, d, E
module seg_3(c0, c1, c2, c3, out3);
    input c0;
    input c1;
    input c2;
    input c3;
    output out3;

    assign out3 =  (~c3 & ~c2 & ~c1 & c0) | (~c3 & c2 & ~c1 & ~c0) |  (c2 & c1 & c0) | (c3 & ~c2 & c1 & ~c0);
endmodule

// Segment 4 of hexdecimal display module
// Activate for: 0, 1, 2, 6, 8, A, b, C, d, E, F
module seg_4(c0, c1, c2, c3, out4);
    input c0;
    input c1;
    input c2;
    input c3;
    output out4;

    assign out4 = (~c3 & c0)  | (~c2 & ~c1 & c0)  | (~c3 & c2 & ~c1);
endmodule


// Segment 5 of hexdecimal display module
// Activate for: 0, 1, 4, 5, 6, 8, 9, A, b, C, E, F
module seg_5(c0, c1, c2, c3, out5);
    input c0;
    input c1;
    input c2;
    input c3;
    output out5;

    assign out5 = (~c3 & ~c2 & c0)  | (~c3 & ~c2 & c1)  | (~c3 & c1 & c0)  | (c3 & c2 & ~c1 & c0);
endmodule

// Segment 6 of hexdecimal display module
// Activate for: 2, 3, 5, 6, 8,9, A, b, d, E, F
module seg_6(c0, c1, c2, c3, out6);
    input c0;
    input c1;
    input c2;
    input c3;
    output out6;

    assign out6 = (c3 & c2 & ~c1 & ~c0)  |  (~c3 & ~c2 & ~c1) | (~c3 & c2 & c1 & c0);
endmodule
