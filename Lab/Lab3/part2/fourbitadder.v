module fourbitadder(LEDR, SW);
	input [8:0] SW;
	output [4:0] LEDR;
	wire c1, c2, c3;

	fulladder fa0 (
		.cout(c1),
		.S(LEDR[0]),
		.B(SW[0]),
		.A(SW[4]),
		.cin(SW[8])
		);

	fulladder fa1 (
		.cout(c2),
		.S(LEDR[1]),
		.B(SW[1]),
		.A(SW[5]),
		.cin(c1)
		);

	fulladder fa2 (
		.cout(c3),
		.S(LEDR[2]),
		.B(SW[2]),
		.A(SW[6]),
		.cin(c2)
		);

	fulladder fa3 (
		.cout(LEDR[4]),
		.S(LEDR[3]),
		.B(SW[3]),
		.A(SW[7]),
		.cin(c3)
		);

endmodule

module fulladder(cout, S, B, A, cin);
	input B, A, cin;
	output cout, S;
	assign s = (A ^ B) ^ cin;
	assign cout = ((A ^ B) & cin) | (~(A ^ B) & B);
endmodule