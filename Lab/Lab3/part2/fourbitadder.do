vlib work
vlog -timescale 1ns/1ns fourbitadder.v
vsim fourbitadder

log {/*}
add wave {/*}

# Extreme Case 0+0=0

force {SW[0]} 0 0, 1 10
force {SW[1]} 0 0, 1 10
force {SW[2]} 0 0, 1 10 
force {SW[3]} 0 0, 1 10
force {SW[4]} 0 0, 1 10
force {SW[5]} 0 0, 1 10
force {SW[6]} 0 0, 1 10
force {SW[7]} 0 0, 1 10
force {SW[8]} 0 0, 1 10
run 20ns

# Two test cases presented here
#    1. First full adder carry into second full adder
#    2. Second full adder carry into third full adder
force {SW[0]} 1 0, 0 10
force {SW[1]} 0 0, 1 10
force {SW[2]} 0
force {SW[3]} 0
force {SW[4]} 1 0, 0 10
force {SW[5]} 0 0, 1 10
force {SW[6]} 0
force {SW[7]} 0
force {SW[8]} 0 
run 20ns

# Two test cases presented here
#    1. Third full adder carry into fourth full adder
#    2. Fourth full adder carry to c_out
force {SW[0]} 0
force {SW[1]} 0 
force {SW[2]} 1 0, 0 10
force {SW[3]} 0 0, 1 10
force {SW[4]} 0
force {SW[5]} 0 
force {SW[6]} 1 0, 0 10
force {SW[7]} 0 0, 1 10
force {SW[8]} 0 
run 20ns

