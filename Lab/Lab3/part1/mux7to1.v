// SW[6:0] 7 signal inputs
// SW[9:7] 3 select lines
// LEDR0 as output

module mux7to1(LEDR, SW);
	input [9:0] SW;
	output [1:0] LEDR;
    reg out;
	
	always @(*) // combinational logic
	begin
		case (SW[9:7])
			3'b000: out = SW[0];
			3'b001: out = SW[1];
			3'b010: out = SW[2];
			3'b011: out = SW[3];
			3'b100: out = SW[4];
			3'b101: out = SW[5];
			3'b110: out = SW[6];
			default: out = SW[7];
		endcase
	end

	assign LEDR[0] = out;

endmodule