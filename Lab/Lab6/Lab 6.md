# Lab 6

## Part I

-   **Q: given the starter code, is the `resetn` signal is an synchronous or asynchronous reset?**
    -   A: It's an synchronous reset.
-   **Q: Is it active high, or active low?**
    -   A: It's active low.
-   **Q: Given this, what do you have to do in simulation to reset the FSM to the starting state?**
    -   A: I have to turn `resetn` into low and input `clock` signal to obtain a positive edge. So, in simulation, this can be done with the following

```
force {KEY[0]} 0 0, 1 10 -r 20
force {resetn} 0
```

-   **State table**

| Prev. State | Input Val. |  \|  | Next State |
| :---------: | :--------: | :--: | :--------: |
|      A      |     0      |  \|  |     A      |
|      A      |     1      |  \|  |     B      |
|      B      |     0      |  \|  |     A      |
|      B      |     1      |  \|  |     C      |
|      C      |     0      |  \|  |     E      |
|      C      |     1      |  \|  |     D      |
|      D      |     0      |  \|  |     E      |
|      D      |     1      |  \|  |     F      |
|      E      |     0      |  \|  |     A      |
|      E      |     1      |  \|  |     G      |
|      F      |     0      |  \|  |     E      |
|      F      |     1      |  \|  |     F      |
|      G      |     0      |  \|  |     A      |
|      G      |     1      |  \|  |     C      |

-   **Completed code**

```verilog
// SW[0] reset when 0
// SW[1] input signal (w)

// KEY[0] clock signal

// LEDR[2:0] displays current state
// LEDR[9] displays output

module sequence_detector(SW, KEY, LEDR);
    input [9:0] SW;
    input [3:0] KEY;
    output [9:0] LEDR;

    wire w, clock, resetn, out_light;
    
    reg [2:0] y_Q, Y_D; // y_Q represents current state, Y_D represents next state
    
    localparam A = 3'b000, B = 3'b001, C = 3'b010, D = 3'b011, E = 3'b100, F = 3'b101, G = 3'b110;
    
    assign w = SW[1];
    assign clock = ~KEY[0];
    assign resetn = SW[0];

    // State table
    // The state table should only contain the logic for **state transitions**
    // Do not mix in any output logic.  The output logic should be handled separately.
    // This will make it easier to read, modify and debug the code.

    always @(*)
    begin: state_table
        case (y_Q)
            A: begin
                   if (!w) Y_D = A;
                   else Y_D = B;
               end
            B: begin
                   if(!w) Y_D = A;
                   else Y_D = C;
               end
            C: begin
                   if(!w) Y_D = E;
                   else Y_D = D;
               end
            D: begin
                   if(!w) Y_D = E;
                   else Y_D = F;
               end
            E: begin
                   if(!w) Y_D = A;
                   else Y_D = G;
               end
            F: begin
                   if(!w) Y_D = E;
                   else Y_D = F;
               end
            G: begin
                   if(!w) Y_D = A;
                   else Y_D = C;
               end
            default: Y_D = A;
        endcase
    end // state_table
    
    // State Register (i.e., FFs)
    always @(posedge clock)
    begin: state_FFs
        if(resetn == 1'b0)
            y_Q <=  A; // Should set reset state to state A
        else
            y_Q <= Y_D;
    end // State Register

    // Output logic
    // Set out_light to 1 to turn on LED when in relevant states
    assign out_light = ((y_Q == F) || (y_Q == G));
    // or assign out_light = ((y_Q == F) || (y_Q == G));

    assign LEDR[9] = out_light;
    assign LEDR[2:0] = y_Q;
endmodule

```

-   **ModelSim**

![img](./image/part1.png)

---

## Part II

$Ax^2 +Bx+C$

**2. Control the datapath**

| High-level Steps                         | Control Signals                          |
| ---------------------------------------- | ---------------------------------------- |
| Initialize: load all data input into registers | `ld_a = 1'b1;` `ld_b = 1'b1;` `ld_c = 1'b1;` `ld_x = 1'b1;` |
| Calculate `A*X`  and load into `RA`      | 1. `alu_select_a = 2'b00;` `alu_select_b = 2'b11;`<br />2. `alu_op = 1'b1;`<br />3. `ld_alu_out = 1'b1; ld_a = 1'b1;` |
| Calculate `AX*X` and load into `RA`      | 1. `alu_select_a = 2'b00;` `alu_select_b = 2'b11;`<br />2. `alu_op = 1'b1;`<br />3. `ld_alu_out = 1'b1; ld_a = 1'b1;` |
| Calculate `B*X` and load into `RB`       | 1. `alu_select_a = 2'b01;` `alu_select_b = 2'b11;`<br />2. `alu_op = 1'b1;`<br />3. `ld_alu_out = 1'b1; ld_b = 1'b1;` |
| Calculate `AXX+C` and load into `RA`     | 1. `alu_select_a = 2'b00;` `alu_select_b = 2'b10;`<br />2. `alu_op = 1'b0;`<br />3. `ld_alu_out = 1'b1; ld_a = 1'b1;` |
| Calculate `AXX+C+BX` and load into `RR`  | 1. `alu_select_a = 2'b00;` `alu_select_b = 2'b01;`<br />2. `alu_op = 1'b0;`<br />3. `ld_r = 1'b1;` |

**3. State diagram**

![img](./image/fsm.svg)

**4. Modified code**

```verilog
module control(
    input clk,
    input resetn,
    input go,

    output reg  ld_a, ld_b, ld_c, ld_x, ld_r,
    output reg  ld_alu_out,
    output reg [1:0]  alu_select_a, alu_select_b,
    output reg alu_op
    );

    reg [3:0] current_state, next_state; 
    
    localparam  S_LOAD_A        = 4'd0,
                S_LOAD_A_WAIT   = 4'd1,
                S_LOAD_B        = 4'd2,
                S_LOAD_B_WAIT   = 4'd3,
                S_LOAD_C        = 4'd4,
                S_LOAD_C_WAIT   = 4'd5,
                S_LOAD_X        = 4'd6,
                S_LOAD_X_WAIT   = 4'd7,
                S_CYCLE_0       = 4'd8,
                S_CYCLE_1       = 4'd9,
                S_CYCLE_2       = 4'd10,
                S_CYCLE_3       = 4'd11,
                S_CYCLE_4       = 4'd12;

    
    // Next state logic aka our state table
    always@(*)
    begin: state_table 
            case (current_state)
                S_LOAD_A: next_state = go ? S_LOAD_A_WAIT : S_LOAD_A; // Loop in current state until value is input
                S_LOAD_A_WAIT: next_state = go ? S_LOAD_A_WAIT : S_LOAD_B; // Loop in current state until go signal goes low
                S_LOAD_B: next_state = go ? S_LOAD_B_WAIT : S_LOAD_B; // Loop in current state until value is input
                S_LOAD_B_WAIT: next_state = go ? S_LOAD_B_WAIT : S_LOAD_C; // Loop in current state until go signal goes low
                S_LOAD_C: next_state = go ? S_LOAD_C_WAIT : S_LOAD_C; // Loop in current state until value is input
                S_LOAD_C_WAIT: next_state = go ? S_LOAD_C_WAIT : S_LOAD_X; // Loop in current state until go signal goes low
                S_LOAD_X: next_state = go ? S_LOAD_X_WAIT : S_LOAD_X; // Loop in current state until value is input
                S_LOAD_X_WAIT: next_state = go ? S_LOAD_X_WAIT : S_CYCLE_0; // Loop in current state until go signal goes low
                S_CYCLE_0: next_state = S_CYCLE_1;
                S_CYCLE_1: next_state = S_CYCLE_2; // we will be done our two operations, start over after
                S_CYCLE_2: next_state = S_CYCLE_3;
                S_CYCLE_3: next_state = S_CYCLE_4;
                S_CYCLE_4: next_state = S_LOAD_A;
            default:     next_state = S_LOAD_A;
        endcase
    end // state_table
   

    // Output logic aka all of our datapath control signals
    always @(*)
    begin: enable_signals
        // By default make all our signals 0
        ld_alu_out = 1'b0;
        ld_a = 1'b0;
        ld_b = 1'b0;
        ld_c = 1'b0;
        ld_x = 1'b0;
        ld_r = 1'b0;
        alu_select_a = 2'b00;
        alu_select_b = 2'b00;
        alu_op       = 1'b0;

        case (current_state)
            S_LOAD_A: begin
                ld_a = 1'b1;
                end
            S_LOAD_B: begin
                ld_b = 1'b1;
                end
            S_LOAD_C: begin
                ld_c = 1'b1;
                end
            S_LOAD_X: begin
                ld_x = 1'b1;
                end
            S_CYCLE_0: begin // RA <- A * X
                ld_alu_out = 1'b1; ld_a = 1'b1; // store in RA
                alu_select_a = 2'b00; // Select RA: A
                alu_select_b = 2'b11; // Select RX: X
                alu_op = 1'b1; // *
            end
            S_CYCLE_1: begin // RA <- AX * X
                ld_alu_out = 1'b1; ld_a = 1'b1; // store in RA
                alu_select_a = 2'b00; // Select RA: AX
                alu_select_b = 2'b11; // Select RX: X
                alu_op = 1'b1; // *
            end
            S_CYCLE_2: begin // RB <- B * X
                 ld_alu_out = 1'b1; ld_b = 1'b1; // store in RB
                 alu_select_a = 2'b01; // Select RB: B
                 alu_select_b = 2'b11; // Select RX: X
                 alu_op = 1'b1; // *
            end
            S_CYCLE_3: begin // RA <- AXX + C
                 ld_alu_out = 1'b1; ld_a = 1'b1; // store in RA
                 alu_select_a = 2'b00; // Select RA: AXX
                 alu_select_b = 2'b10; // Select RC: C
                 alu_op = 1'b0; // +
            end
            S_CYCLE_4: begin // RR <- AXX + C + BX
                 ld_r = 1'b1; // store in RR
                 alu_select_a = 2'b00; // Select RA: AXX + C
                 alu_select_b = 2'b01; // Select RB: BX
                 alu_op = 1'b0; // +
            end
        // default:    // don't need default since we already made sure all of our outputs were assigned a value at the start of the always block
        endcase
    end // enable_signals
   
    // current_state registers
    always@(posedge clk)
    begin: state_FFs
        if(!resetn)
            current_state <= S_LOAD_A;
        else
            current_state <= next_state;
    end // state_FFS
endmodule
```



**5. Generated FSM**

![img](./image/part2q5.1.png)

![img](./image/part2q5.2.png)

**6. ModelSim**

![img](./image/part2q6.png)

---

##### Part III



**1. Schematic**



**2. State diagram**



**3. Controller schematic**



**4. Top-level schematic**



**6. ModelSim**

