# CSC258 Midterm Sample Questions

---

**Simplify x+x![ y bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)+![ y bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)+ xz algebraically (not using truth tables).**

Answer: 
x+x![ y bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)+![ y bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)+xz 
= x+![ y bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)+xz 
= x+xz+![ y bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif) 
= x+![ y bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/ybar.gif)

------

**What is the output sequence of the following "counter", after it gets established in its cycle?**

![img](./image/midctr1.gif)

Answer: 1, 2, 3, 1, 2, 3, ...

(or any other equivalent sequence, since it is infinite in both directions; e.g. 2, 3, 1, 2, 3, 1, ...)

(of course you can write the numbers in binary instead, and indeed you might find that more natural: 01, 10, 11, 01, 10, 11, ...)

------

**As an eight-bit binary number, 12 is 00001100. As an eight-bit binary number, 18 is 00010010. Show how we subtract 18-12 by adding the two's-complement of 12.**

Answer: 
Flip 00001100 and add 1, to get a representation of -12:

```
11110011
+      1
--------
11110100

```

Add 18 plus this -12 value:

```
00010010
11110100
--------
00000110

```

which is 6.

------

The hypothetical (and admittedly somewhat silly) XY flip-flop has the following truth table:

| X    | Y    | Qn+1                                     |
| ---- | ---- | ---------------------------------------- |
| 0    | 0    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) |
| 0    | 1    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) |
| 1    | 0    | 1                                        |
| 1    | 1    | 0                                        |

Just as we designed a circuit for the JK flip-flop based on a master-slave SR flip-flop, design a circuit for the XY flip-flop based on the JK flip-flop. Formulate expressions for J and K in terms of Qn, X, and Y, and draw a circuit. Recall that in the JK flip-flop, J is the "set" line.

(N.B. your expressions for J and K need not necessarily mention *all* of Qn, X, and Y.)

Answer:

J = ![(XY) bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/bigxybar.gif) 
K = ![X bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/bigxbar.gif)+Y

![img](./image/midxy1.gif)

**Detailed explanation of the answer** 
(some of this might appear in your answer on a midterm, or not; the above would be worth the full marks):

We can augment the above table by deciding whether we want to "set", "reset" (clear), or "toggle":

| X    | Y    | Qn+1                                     | action |
| ---- | ---- | ---------------------------------------- | ------ |
| 0    | 0    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) | toggle |
| 0    | 1    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) | toggle |
| 1    | 0    | 1                                        | set    |
| 1    | 1    | 0                                        | reset  |

This, then, gives us desired values for J and K:

| X    | Y    | Qn+1                                     | action | J    | K    |
| ---- | ---- | ---------------------------------------- | ------ | ---- | ---- |
| 0    | 0    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) | toggle | 1    | 1    |
| 0    | 1    | ![Q<sub>n</sub> bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/qnbar.gif) | toggle | 1    | 1    |
| 1    | 0    | 1                                        | set    | 1    | 0    |
| 1    | 1    | 0                                        | reset  | 0    | 1    |

And that leads to the above two equations, J = ![(XY) bar](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/bigxybar.gif) and K = ![X bar ](http://www.teach.cs.toronto.edu/~ajr/258/mid/midsample/bigxbar.gif)+Y.

---

**What function does the following logic gate diagram compute?**

![img](http://www.teach.cs.toronto.edu/~ajr/258/mid/2004soln/mid04comb0.gif) 
Answer: ![img](http://www.teach.cs.toronto.edu/~ajr/258/mid/2004soln/mid040q1as.gif)

**Simplify this formula (using any appropriate technique).**

Answer: 
![img](http://www.teach.cs.toronto.edu/~ajr/258/mid/2004soln/mid040q1bs.gif)

**Draw a logic gate diagram for your simplified formula.**

------

**Draw a sequential circuit with three outputs and one input line in addition to the clock. While the data input is 0, your circuit functions as a three-bit counter (counting clock pulses). While the data input is 1, your circuit skips the value 010 (it goes from 001 to 011, but all other transitions are the same). When the data input goes back to 0, the count continues (it doesn't jump back for a missed 010 or anything like that).**

Answer: 
![img](http://www.teach.cs.toronto.edu/~ajr/258/mid/2004soln/mid04seq1s.gif) 
The clock is not shown above; it is wired into the input of each flip flop.****

------

**Using four-bit numbers, show how the addition of 3+(-2) in the signed representation is the same as adding 3+14 in the unsigned representation. What is the value of the result?**

Answer: 
3 is 0011 
-2 is 1110 
14 is also 1110

```
 0011
+1110
 ----
 0001

```

(probably best to show the carries too)

The result is 1. This is the correct value for 3+(-2); it represents an overflow of 3+14, where the result is congruent to the right answer mod 16 (24).

------

