# Boolean Basics

[TOC]

---

## Operators

*4 common Boolean operators*

### AND

| $p$  | $q$  | $p\cdot q$ |
| ---- | ---- | ---------- |
| 0    | 0    | 0          |
| 0    | 1    | 0          |
| 1    | 0    | 0          |
| 1    | 1    | 1          |



### OR

| $p$  | $q$  | $p+ q$ |
| ---- | ---- | ------ |
| 0    | 0    | 0      |
| 0    | 1    | 1      |
| 1    | 0    | 1      |
| 1    | 1    | 1      |



### NOT

| $p$  | $\overline{p}$ |
| ---- | -------------- |
| 0    | 1              |
| 1    | 0              |



### XOR

Note: Whenever there is one input in high, then the output is the **toggle** of the other input.

| $p$  | $q$  | $p\oplus q$ |
| ---- | ---- | ----------- |
| 0    | 0    | 0           |
| 0    | 1    | 1           |
| 1    | 0    | 1           |
| 1    | 1    | 0           |

### Other

NAND, NOR, XNOR, Neg-AND, Neg-OR, etc.

---

## Boolean Algebra

### Identity Law

$x\cdot 1=x$  

$x+0=x$ 

### Base Law

$x\cdot0=0$  

$x+1=1$ 

### *Idempotence

$x\cdot x=x+x=x$ 

### Excluded middle

$x+\overline{x}=1$ 

Note: $xy + x\overline{y} = x$ 

### Non-contradiction

$x\cdot\overline{x}=0$ 

### Double-negation

 $\overline{\overline{x}}=x$ 

### Axioms

$0\cdot 0=0$  

$1\cdot 1=1$ 

 $0\cdot 1=1\cdot 0 = 0$ 

 $\overline{1} = 0$

### XOR Gate definition

$x\oplus y = x\cdot \overline{y}+\overline{x}\cdot y$ 

### Commutative Law

$x\cdot y=y\cdot x$  

$x+y=y+x$ 

 $x \oplus y = y \oplus x$ 

### Associative Law

$x\cdot(y\cdot z)=(x\cdot y)\cdot z$ 

$x+(y+z)=(x+y)+z$ 

$x \oplus (y \oplus z)=(x \oplus y) \oplus z$ 

### Distributive Law

$x\cdot(y+z)=x\cdot y + x\cdot z$  

$x+(y\cdot z)  = (x+y) \cdot (x+z)$

### *Consensus Law

$x\cdot y+\overline{x}\cdot z+y\cdot z = x\cdot y + \overline{x}\cdot z$ 

### *Absorption Law

$x\cdot (x+y) = x$ 

$x+(x\cdot y)=x$ 

$x+(\overline{x}\cdot y)=x+y$ 

### De Morgan's Law

$\overline{x}\cdot\overline{y}=\overline{x+y}$ 

$\overline{x}+\overline{y}=\overline{x\cdot y}$ 

---

## Proof of Absorption Law

>   In general in this course, you only need cite a law if it's not obvious what you're doing. The lines in a proof have to follow from each other in accordance with these laws, but usually it will be obvious to the reader what laws you're using; you only need cite laws if clarification is needed. This principle is followed below. (If you're uncertain, feel free to cite more than you have to.)

Let us first prove the *second* absorption law, that $a+ab = a$.

One way to do this is to begin with one side of the equation, then argue that it is equal to something, which is equal to something else, and so on, eventually which is equal to the other side of the equation. By the transitivity of equality, this proves that the two sides of the equation are equal.

**Proof:**

```
  a + ab
= a*1 + ab
= a(1+b)
= a*1
= a

```

Now let us prove the *first* absorption law, that a(a+b) = a.

```
  a(a+b)
= a*a + a*b
= a + ab
= a  (by absorption law #2)

```

I had to prove the second absorption law first so that I could use it in the proof of the first absorption law.

Finally, the third absorption law, that a+nota*b = a+b (to simplify this web page a bit, I'll use "nota" to mean not a, i.e. "a" with an overbar):

```
  a + nota*b
= (a+nota)(a+b)   (by the weirder distributive law: distribute OR over AND)
= 1(a+b)
= a+b
```

>   Note that proofs are much easier to read (verify) than to create. The verification of a boolean algebra proof can even be done mechanically, in fact. The formulation of the proof is a creative process and is considerably harder.
>
>   That is to say, the fact that you can understand a proof doesn't mean that you could have come up with one yourself. Make sure you get some practice in formulating boolean algebra proofs, not just in reading and understanding *my* proofs.

---

## Proof of Consensus Law

$x\cdot y+\overline{x}\cdot z+y\cdot z = x\cdot y + \overline{x}\cdot z$

**Proof:**

  xy + x'z + yz

=xy + x'z + yz*1 (by identity law)

=xy + x'z + yz*(x+x') (since x+x'=1)

=xy + x'z + xyz + x'yz

=(xy+xyz)+(x'z+x'yz)

=xy(1+z)+x'z(1+y)

=xy+x'z (by base law)

