# Sequential Circuits

[TOC]

---

## Creating sequential circuits

Essentially, sequential circuits are a result of having **feedback** in the circuit.

### Gate Delay

Even in combinational circuits, outputs don’t change instantaneously. There is **Gate Delay** or **Propagation Delay**, which is the length of time it takes *for an input change to result in the corresponding output change*.

### Feedback Circuit

Some gates don’t have useful results when outputs are fed back on inputs. For example, if we connect the output as one of its input of an AND gate, if the actual input A=0, then no matter what Q~t~ was, Q~t+1~ becomes 0; then, for the next stage, Q~t~=0. So no matter what A was, Q~t+1~ was always 0 (stuck). Similarly, OR gate has this problem when A=1.

But **NAND**, **NOR** gates with feedback have more interesting characteristics, which lend themselves to storage devices. Unlike the AND and OR gate circuits (which get stuck), the output Q~t+1~ can be changed, based on A.

For NAND gate, let’s assume we set A=0. Then, current output Q~t~ will go to 1. If we leave A unchanged we can store 1 indefinitely! (constantly output 1) If we set A=1, Q’s value can change, but there’s a catch! (NOR gate it's similar when A=0) This is an **Unsteady state**. We can’t store 0 long. We want to avoid this. We should be able to store high and low values for as long as we want, and change those values as needed.

|  $A$  | $Q_T$ | $Q_{T+1}$ | <- NAND \| NOR-> |  $A$  | $Q_T$ | $Q_{T+1}$ |
| :---: | :---: | :-------: | :--------------: | :---: | :---: | :-------: |
|   0   |   0   |     1     |        \|        | **0** |   0   |     1     |
|   0   |   1   |     1     |        \|        | **0** |   1   |     0     |
| **1** |   0   |     1     |        \|        |   1   |   0   |     0     |
| **1** |   1   |     0     |        \|        |   1   |   1   |     0     |

See more [truth tables for common gates](./boolean.md).

### Latches

If multiple gates of these types are combined, you can get more **steady** behaviour, which are called **latches**.

| $\overline{S}$ | $\overline{R}$ |   $Q_T$   | $\overline{Q_T}$ | $Q_{T+1}$ | $\overline{Q_{T+1}}$ |
| :------------: | :------------: | :-------: | :--------------: | :-------: | :------------------: |
|     ~~0~~      |     ~~0~~      |   ~~x~~   |      ~~x~~       |   ~~1~~   |        ~~1~~         |
|       0        |       1        |     x     |        x         |     1     |          0           |
|       1        |       0        |     x     |        x         |     0     |          1           |
|   ==**1**==    |   ==**1**==    | ==**0**== |    ==**1**==     | ==**0**== |      ==**1**==       |
|   ==**1**==    |   ==**1**==    | ==**1**== |    ==**0**==     | ==**1**== |      ==**0**==       |

**$\overline{S}\overline{R}$ latches**

$\overline{S} $ and $ \overline{R}$ are called "set" and "reset" respectively. Note how the circuit “remembers” its signal when going from 10 or 01 to 11 (highlighted). Going from 00 to 11 produces **unstable** behaviour. It depends on which input changes first. (i.e. it can be 01 or 10 and we don't know.)

Important to note that the output signals don’t change instantaneously, which is one of the cause for unstable behaviour.

### Instability

Unstable behaviour occurs when a $\overline{S}\overline{R}$ latch’s inputs go from 00 to 11, or a $SR $ latch’s inputs go from 11 to 00. The signals don’t change simultaneously, so the outcome depends on which signal changes **first**. Because of the unstable behaviour, 00 is considered a **forbidden state** in NAND-based $\overline{S}\overline{R}$ latches, and 11 is considered a forbidden state in NOR-based $SR $ latches. 

### Clock

We already have latches to store value. Now we need some sort of timing signal, to let the circuit know when the output may be sampled, which is **clock signals**.

#### Signal restrictions

**Frequency** = how many pulses occur per second, measured in Hertz (or Hz). The frequency limit of a latch circuit can be sampled is determined by:

-   lantency time of transistors: setup time & hold time
-   Setup time for clock signal
    -   Jitter
    -   Gibbs phenomenon

#### Clocked latches

![img](./image/srff.gif)

|   C   |   S   |   R   |  \|  | Q~T+1~ |    Result     |
| :---: | :---: | :---: | :--: | :----: | :-----------: |
|   0   |   x   |   x   |  \|  |  Q~T~  |   no change   |
|   1   |   0   |   0   |  \|  |  Q~T~  |   no change   |
|   1   |   0   |   1   |  \|  |   0    |     reset     |
|   1   |   1   |   0   |  \|  |   1    |      set      |
| ~~1~~ | ~~1~~ | ~~1~~ |  \|  | ~~?~~  | ~~Undefined~~ |

Adding another layer of NAND gates to the $\overline{S}\overline{R}$ latch gives us a **clocked SR latch** or gated SR latch. It's basically, a latch with a control input signal C. The input C is often connected to a pulse signal that alternates regularly between 0 and 1 (clock)

If clock is **high**, the first NAND gates invert those values, which get inverted again in the second NAND gates. Start off with S=0 and R=1, reseting Q=0. Then, setting both inputs to 0 maintains the output values.

If clock is **low**, even if the inputs change, the low clock input **prevents** the change from reaching the second stage of NAND gates. (the output of the first NAND gates are all 1s) 

So the clock needs to be **high** in order for the inputs to have any effect.

#### Indeterminate state problem

Assuming the clock is 1, we still have the racing problem when S and R are both 1, since the state of Q is indeterminate. A better design would be to **prevent** S and R from **both going high**.

![img](./image/dff.png)

One solution is **D latch**. (D for Data) By making the inputs to R and S dependent on a single signal D, you avoid the indeterminate state problem. The value of D now sets output Q low or high whenever C is high. This design is good, but still has problems: timing issues.

#### Latch timing issues

When the clock signal is high, the output of the D latch keeps toggling back and forth. So we say D latch is **transparent**. Transparent means that any changes to its inputs are **visible** to the output when control signal (Clock) is 1. 

But the **key** issue is that the output of a latch should **not** be applied directly or through combinational logic to the input of the same or another latch when they all have the same control (clock) signal.

So, we want output change **only once** when the clock pulse changes. The solution is to create **disconnect** between circuit output and circuit input, to **prevent unwanted feedback** and changes to output. (i.e. Flip-flop)

---

## Flip-flop

A **flip-flop** is a latched circuit whose output is triggered with the **rising edge** or **falling edge** of a clock pulse. Example: The SR master-slave flip-flop. (the first one is master for controlling input; the second one is slave. It is triggered by negedge)

![img](./image/master_slave_srff.gif)

### D flip-flop

But SR flip-flops still have the same issues of unstable behaviour as SR latches. The solution is **D flip-flop**. It basically connect a D latch to the input of a SR latch. It's also Negative-edge triggered. (like the SR flip-flop)

![img](./image/dsrff.gif)

If the clock signal is high, the input to the first flip-flop is sent out to the second. The second flip-flop doesn’t do anything until the clock signal goes down again. When it clock goes from high to low, the first flip-flop stops transmitting a signal, even if the input to D changes, and the second one starts. Once the clock goes high again, the first flip-flop starts transmitting the new D value. At the same time, the second flipflop stops.

### T flip-flop

![img](./image/tff.gif)

Like the D flip-flop, except that it **toggles** its value whenever the input to T is **high**.

### JK flip-flop

![img](./image/jkdff.jpg)



Takes advantage of all combinations of two inputs (J & K) to produce four different behaviours:

-   if J and K are 0, maintain output.
-   if J is 0 and K is 1, set output to 0.
-   if J is 1 and K is 0, set output to 1.
-   if J and K are 1, toggle output value.

------

## Explaination for stability of sequential circuit states

>   A question in assignment two asks you to list stable output states for a sequential circuit for all possible input combinations, and asks whether it could be used as a one-bit data latch.

Consider a standard SR latch.

![stable](./image/stable.gif)

Let's name the top output "Q~a~" and the bottom one "Q~b~". We don't know in advance that the bottom output is always the negation of the top output; in fact, this is not always the case when the latch is not used "properly" (e.g. if S=R=1 sometimes).

-   For inputs S=R=0, let us consider all four possible outputs and see whether they are stable states.
    -   If Q~a~=Q~b~=0, then the NOR gates' inputs are all 0, thus their outputs will be 1 (thus changing Q~a~ and Q~b~). Thus this is **not a stable state**. ("not a stable state" means that it won't **stay** in that state) It doesn't really matter what else right now, as we'll be looking at that other state in its turn. (It also doesn't matter if it ends up coming back to Qa=Qb=0 later, because we've just established that it's not going to stay there.)
    -   If Q~a~=0 and Q~b~=1, then if you look at the inputs to the NOR gates and their supposed output, you will see that you have a match. This is a stable state because it is possible for it to stay that way. 
    -   Similarly, Q~a~=1 and Q~b~=0 is a stable state (for inputs S=R=0).
    -   If Q~a~=Q~b~=1, then the inputs to the NOR gates are each 0 and 1, and so their outputs will be 0, so this is not a stable state.

    In summary, for inputs S=R=0, the stable states are 0,1 and 1,0.

-   Suppose that S=1 and R=0. We can see that the Q~b~ NOR gate will necessarily output 0, since an input of 1 is a base law for OR; that is, the OR is 1, so the NOR is 0. Thus any stable state will have Q~b~=0.

    Since any stable state will have Q~b~=0, we know what the inputs to the Q~a~ NOR gate will be: R=0 and Q~b~=0. So that NOR gate will output 1, i.e. Q~a~=1.

    Thus the only possible stable state is Q~a~=1 and Q~b~=0. But this does not establish that Q~a~=1 and Q~b~=0 *is* a stable state; we've merely shown that all other output states are *not* stable.

    But in fact if you look at the inputs to each of the NOR gates for S=1 and R=0 and Q~a~=1 and Q~b~=0, you will see that this is indeed a stable state. The Q~a~ NOR gate has inputs 0 (R) and 0 (Q~b~), so it will output 1, which is what is already there; and the Q~b~ NOR gate has inputs 1 (S) and 1 (Q~a~), so it will output 0, which is what is already there. It won't change; it's stable.

    So for inputs S=1 and R=0, there is only one stable state, which is Q~a~=1 and Q~b~=0.

-   The case S=0 and R=1 is just the vertically-flipped mirror image of the case S=1 and R=0, so from the above , we know that the only stable output state is Q~a~=1 and Q~b~=0.

    So for inputs S=0 and R=1, there is only one stable state, which is Q~a~=0 and Q~b~=1.

-   This leaves the case S=R=1. In this case we see that the NORs will both output 0, by the above-mentioned base law. Furthermore, if you trace those outputs around back to the NOR inputs, you'll see that the NOR gates each have one input of 1 and one input of 0, and 1 NOR 0 = 0, so this is a stable state, and the only one.

    So for inputs S=R=1, there is only one stable state, which is Q~a~=Q~b~=0.

>   So, can we use this as a one-bit memory? Yes, if we avoid the input state S=R=1. If S and R are usually zero, then if you raise one of them to 1 and then return it to 0 after the circuit stabilizes, then when S=R=0 again you will see the outputs 1,0 iff it was S you raised to 1 and the outputs 0,1 iff it was R you raised to 1, and both of these output states are stable for S=R=0.

---

## Sequential circuit design

### Timing Considerations

Input should **NOT** be changing at the same time as the active edge of the clock.

-   **Setup Time:** Input should be stable for some time **before** active clock edge.
-   **Hold Time:** Input should be stable for some time immediately **after** the active clock edge.

#### Maximum clock frequency

Time period between two active clock edges cannot be shorter than
 **longest propagation delay** between any two flip-flops + **setup time** of the flip-flop.

#### Resetting inputs

Since flip-flops have unknown state when they are first powered up, we need a convenient way to **initialize** them. Reset signal (e.g. reset_n) is a signal that resets the FF output to 0. This is unrelated to R input of SR latch.

-   **Synchronous reset:** the output is reset to 0 only on the **active edge** of the clock.
-   **Asynchronous reset:** the output is reset to 0 **immediately** (as soon as the asynchronous reset signal becomes active), **independent** of the clock signal.

### Counters

See more on [old course website](http://www.teach.cs.toronto.edu/~ajr/258/notes/seq/).

#### Ripple counter

![img](./image/ripple.gif)

**Problems:** 

-   Since each JKFF is clocked to the previous, the i^th^ bit only *starts* flipping **after** the (i-1)^th^ bit *finishes* flipping. In a wide counter (e.g. 32 or 64 bits), this means we can't run it very fast; we have to allow sufficient time for all the signals to propagate. This goes against our modern trends of trying to build computers which are simultaneously faster *and* have a larger word-size.
-   Timing isn’t quite synchronized with the rising clock pulse. (unreliable for timing)

The solution to these problems is called a "synchronous counter" (synchronous meaning "all at the same time"), shown next.

#### Synchronous counter

The input to each JK flip-flop will be an AND of the Qs of all of the lower-order flip-flops. The clock input  goes to all flip-flops.

![img](./image/synchronous.gif)

Then all of the flip-flops flip **simultaneously** (if they should be flipping at all this cycle. The cumulative AND function computed on the right is a *predictor* function, which determines in advance whether or not the given bit should flip in the next cycle.

What about the "reset" function? See my synchronous counter with reset, next.

#### Synchronous counter with reset

If you imagine using a counter, such as in a stopwatch or a geiger counter, you need to have not only the "add one" input but also a "reset" input. We can have a **synchronous reset** or an **asynchronous reset**. An asynchronous reset takes effect immediately. A synchronous reset has to be held down for one clock pulse.

The synchronous reset can seem a bit odd, in that the clock pulse is actually the "add one" line. So while the "reset" input is 1, the "add one" operation yields 0, rather than x+1. But a 'reset' by itself does nothing without a clock pulse.

![img](./image/syncsync.gif)

**Analysis:** 

-   consider the 'reset' line is zero. (we'll deal with the unusual top flip flop separately, afterwards.) If 'reset' is zero, the 'J' inputs are being ANDed with 1, and the 'K' inputs are being ORed with 0. Each of these is the identity operation, so you simply have the regular synchronous counter

    Therefore when 'reset' is zero, this functions as a simple synchronous counter.

-   consider when 'reset' is one (still ignoring the unusual top flip flop for now). The 'J' inputs are ANDed with 0, and the 'K' inputs are ORed with 1. This is the *base* law operation for those values: J will be 0 no matter what the other input is, and K will be 1 no matter what the other input is. This is the reset operation for an SR or JK flip-flop: the value of the flip-flop gets set to zero. 

    So when the 'reset' input line is 1, a reset of all flip-flops occurs.

-   But what about the top flip-flop (representing the least-significant bit)? 

    -   When 'reset' is zero, its inputs should be 1,1, because this bit always flips. 
    -   When 'reset' is one, its inputs should be 0,1, just like they should be for the other flip-flops. So the 'K' input should always be 1, and the 'J' input should be not-reset.

### Registers

See more on [old course website](http://www.teach.cs.toronto.edu/~ajr/258/notes/seq/).

#### Simple register (parallel load)

![img](./image/register.gif)

The above mechanism could be called a "parallel load". Every flip-flop gets loaded at the same time.

#### Simple shift register (serial load)

Here we load the register one bit at a time; the SHIFT control line moves everything to the left by one bit, losing the former top bit, and gaining a new low bit.

![img](./image/shiftregister.gif)

However, we want to be able to do the straightforward "parallel load" to load completely new contents into a register in one cycle, rather than 32 cycles for a 32-bit register. But the SHIFT facility is also useful. For example, it constitutes multiplying by two.

So we often design a circuit which can shift *or* load, next.

#### Designing a circuit which can shift or load

![img](./image/shiftandload.gif)

Conspicuously absent from this picture is the clock. We could OR together the SHIFT and LOAD lines and feed that into each flip-flop's clock. But we'd like to move towards being able to synchronize with an external clock, so that the entire circuit, of which our register will be just one part, can advance synchronously.

So let's move on to the fully-functional register, next.

#### More realistic shift register with parallel load

![img](./image/allsinging.gif)

Each DFF is either set to

-   x~i~ (if LOAD), or
-   x~(i-1)~ (if SHIFT), or
-   y~i~ (if (not SHIFT) AND (not LOAD))

, every cycle. So the clocks of all of the D flip-flops can be connected into the system master clock.

In fact, we would in practice make this even *more* complex, by having separate "left-shift" and "right-shift" control lines. The above SHIFT is a left-shift.

(You are not expected to be able to reproduce the above from memory in a test or exam situation, although designing such a thing should not be beyond you by the end of the course.)

---

## Additional Problems

\1. Design a three-bit "counter" which counts 0, 3, 6, 1, 4, 7, 2, 5, 0, 3, ... .

(Note: This can be multiple problems, with different design constraints. E.g. use D flip-flops (probably most obvious), use JK flip-flops with both inputs the same, or use SR flip-flops.)

[sample solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans1.html)



\2. Design a three-bit "counter" which counts 0, 1, 2, 4, 5, 7, 0, ... .

(Also can be considered to be several different problems.)

[sample solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans2.html)



\3. To turn any counter into a "count-down" counter (i.e. a two-bit "count-down" counter will count 11, 10, 01, 00, 11, ...), you can XOR each of the output bits with 1, thus inverting them.

XORing with 0, of course, doesn't change the output.

Draw a two-bit counter with inputs "up" and "down" (in addition to the common clock). A third flip-flop stores whether or not the counter should currently be counting up or down; the "up" input makes the clock start counting up and the "down" input makes the clock start counting down. (The "up" or "down" input will be held at 1 for at least a clock cycle, and it doesn't matter what happens *while* it's being held at 1.)

[sample solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans3.html)



\4. Here's a cute one: 
Change an n-bit counter (a normal one, which counts in order) to one which counts down rather than up, just by rewiring it, adding **no** additional circuitry of any kind (no additional gates).

(This problem is a "trick", obviously; it's ok if you don't get it, and there's no way I could ask something like this on a midterm or exam.)

[solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans4.html)



\5. Design a hardware stack to hold a maximum of four values, each consisting of one bit. The stack has the following input lines:

-   Clock
-   POP
-   PUSH
-   input data

and a single output line. The output line always reports the current top of the stack.

(Recall that a stack pushes data onto the top of the stack, and pops data from the top of the stack. In other words, last in, first out. If more than four values are pushed on, older values are discarded.)

You may use AND, OR, XOR, and NOT gates, etc; flip-flops of any kind; and/or decoders, multiplexers, etc.

Optional hint: There is no need to keep track of how many items are on the stack. Just keep four D flip-flops for the up-to-four items on the stack. The initial values won't matter unless more things are popped off the stack than are pushed, and this is not allowed.

[sample solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans5.html)



\6. Design a hardware queue. This question looks rather similar to question five above, and I admit that it is quite similar in some ways; on the other hand, it has some notable differences.

The queue has the following input lines:

-   Clock
-   CLEAR
-   ADD
-   LEAVE
-   input data

and a single output line. The output line always reports the current front of the queue. CLEAR is used to set the queue to empty.

(Recall that in a queue, data is added to the end of the queue, and data leaves from the front of the queue. In other words, first in, first out.)

Your queue will hold a maximum of two values, each consisting of one bit. If there are already two values in the queue and another is added, we don't care what happens, as this is a prohibited action. Similarly, we don't care about the output value when there is an empty queue, nor about what happens once more LEAVEs than ADDs have been asserted since the last CLEAR; finally, you needn't be concerned with what will happen when your circuit is initially powered on, as the first operation will be a CLEAR, and you can assume that no more than one of the control lines will be asserted at a time. (These are just examples; in general, we don't care what happens if the queue is used incorrectly.)

You may use AND, OR, XOR, and NOT gates, etc; flip-flops of any kind; and/or decoders, multiplexers, etc.

Optional hint: In addition to the data-holding flip-flops, use two JK flip-flops to form a two-bit counter which keeps track of how many items are in the queue.

[sample solution](http://www.teach.cs.toronto.edu/~ajr/258/probs/seq/ans6.html)



\7. For this question your client is a radio DJ. At this radio station, they want to use a circuit you will design to help them remember to announce the time on the radio at least every 30 minutes.

There will be two time displays: one is the current time, in the form HH:MM; the other is the number of minutes since the time was last announced. There is also a single output LED which blinks when the amount of time since they last announced the time is greater than 25 minutes. It should go on and off with a period of two seconds (i.e. on for one second, off for one second, repeat).

The input consists of a single line, which comes from a button they press when they announce the time. That is, this resets the second time display to zero. There is also a clock pulse which runs at the rate of 16 cycles per second.

For the purposes of this assignment question, suppose that the time on the main display coincidentally happens to have the correct time when the circuit is first powered on. Also, someone will press the push-button soon, and they know that the second time display is not reliable before then.

Use high-level components in answering this question. For example, don't draw a four-bit counter using JK flip-flops; draw a box and label it "four-bit counter".

You must include six seven-segment displays (four for the HH:MM display and two for the second display which shows the number of minutes), and they will be connected to the output of seven-segment decoders.

(This question is rather open-ended, so I have less to say on the matter of a sample solution, and I won't be posting one. If you have a solution you want me to look at, please feel free to bring it by during office hours. You can also ask me questions about the problem, or about a "correct" answer.)

---