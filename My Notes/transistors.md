# Transistors

[TOC]

---

## Goal

-   Properties of [electricity](#electricity) & understand basic principles of electric circuits.
-   Illustrate how doped[^doping] [semiconductor](#semiconductors) materials can be configured to create transistors ([p-n junctions](#p-n junctions)).
-   Combine transistors to [create logic gates](#making gates).

## Structure

```mermaid
graph LR
A[Transistors]
B[p-n Junctions]
C[Semiconductors]
D[Electricity]
A --> |based on| B
B --> |made from| C
C --> |conduct| D
```

## Electricity

Electricity is the flow of charged particles (usually electrons) through a material. These charged particles come from atoms, which are made up of protons[^Protons], neutrons [^Neutrons] and electrons [^Electrons].

(-) => (+) The current always flows from source[^High electrical potential] to the zero voltage point of a circuit[^Low electrical potential] [^Ground]. There are 2 common sources of electricity: batteries[^Batteries], electrical outlets. The direction of electricity is the movement of the **positive charges**.

## Semiconductors[^Semiconductors]

Electricity always like to take the path of least resistance. Electricity can flow freely through a solid if there are free **valence** electrons in the outer layer after the solid is formed. Semiconductors are solid and stable at room temperature, but energy can make electrons from the valence layer become loose. 

Silicon has 4 electrons in its valence electron layer. Each atom wants 8, forms a lattice with its neighbours. To encourage the semiconductor’s conductivity, impuritiesare introduced in the fabrication process, to increase the number of free charge carriers.  This process is also referred to as doping[^Doping] the semiconductor.

-   n-type semiconductors: adding elements from group 15, which have 5 electrons in its valence layer (e.g. **phosphorus** 磷, **arsenic** 砷).  The carriers are electrons that are not bound to the solid, and can flow more freely through the material. 
-   p-type semiconductors: adding elements from group 13, which have 3 electrons in its valence layer (e.g. **boron** 硼). The carriers are called holes, to represent the electron gap as a particle as well.

(n => p) The electrons at the surface of the n-type material are drawn to the holes in the p-type.

## p-n Junctions

![img](./image/forwardbias.png)

When left alone, the electrons from the n section of the junction will mix with the holes of the p section (n => p), cancelling each other out, and creating a particle-free section called **the ==depletion== layer**. Once this depletion layer is wide enough, the doping atoms that remain will create an **electric field** in that region.

An electric field is created when a charge difference exists between two regions. Any electrons[^Electrons] in the middle would be attracted to the positive side and repelled by the negative side. When a phosphorus atom loses its electron, that atom develops an overall positive charge. Similarly, when a boron atom takes on an extra electron, that atom develops an overall negative charge. (n-type - => +) (p-type + => -) This creates an electric field in the depletion layer. So, a depletion layer is made up of many of these electrically imbalanced phosphorus and boron atoms. 

The electric field caused by these atoms will cause holes to flow back to the p section (p-type + => - => +) , and electrons to flow back to the n section (n-type - => + => -). 

-   The current caused by this ==electric field== is called **drift**. (i.e.  n-type - => + => - )
-   The current caused by the ==initial electron==/hole recombination is called **diffusion**. (i.e. n-type - => + )

 At rest, these two currents reach **equilibrium**.

## Forward Bias & Reverse Bias

-   **Forward bias (+) ->|pn|<- (-)**
    When a positive voltage is applied to the p end of this junction, electrons are injected into the n-type section. This **narrows** the depletion layer and increasing the electron diffusion rate. With a smaller depletion layer, the electrons travel more easily through to the p-type section, and back into the other terminal of the voltage source.
-   **Reverse bias (-) |p <--> n| (+)**
    When a positive voltage is applied to the n side of the junction, the depletion region at the junction becomes **wider**, preventing the carriers from passing.  A small current still flows through the circuit, but it is weak and does not increase with an increase in the applied voltage. 
-   So when a junction is forward biased, it becomes like a virtual **short-circuit**, and when the junction is reverse biased, it becomes like a virtual **open-circuit**.

## Common Transistors

Transistors use the characteristics of p-n junctions. There are 3 main types:

-   BJT -  Bipolar Junction Transistors 
-   MOSFET - Metal Oxide Semiconductor Field Effect Transistor 
-   JFET - Junction Field Effect Transistor 

## MOSFET

![mosfet](./image/mosfet.gif)

MOSFET (Metal Oxide Semiconductor Field Effect Transistor) are composed of a layer of semiconductor material, with two layers on top of the semiconductor: 

-   An **oxide** layer that doesn’t conduct electricity, 
-   A **metal** layer (called the **gate**), that can have an electric charge applied to it

These are the M and O components of MOSFETs.

The semiconductor sections are two pockets of n-type material, resting on a substrate layer of p-type material. 

A voltage (V_DS) is applied across the two n-type sections, called the **drain** and the **source**. No current will pass between them though, because the p section in between creates at least one [**reverse-biased p-n junction**](#Forward Bias & Reverse Bias).

However, when a voltage (V_GS) is applied between the **source** and the metal plate (the **gate**), positive charges are built up in the metal layer, which attracts a layer of negative charge to the surface of the p-type material. 

This layer of electrons creates an **n-type channel** between the drain and the source, connecting the two and allowing current to flow between them.  The wider the channel, the higher the current.

There are 2 types of MOSFETs exist, based on the semiconductor type in the drain and source, and the channel formed. 

![nmos-pmos](./image/nmos-pmos.png)

-   **nMOS transistors** (the design described so far) conduct electricity when a positive voltage (5V) is applied to the gate.
-   **pMOS transistors** (indicated by a small circle above the gate) conduct electricity (i.e., act as a closed switch) when the gate voltage is logic-zero.

![img](./image/mosfets.gif)

MOSFETs can make current flow, based on the voltage values in the **gate** & **drain**.

| V_DS | V_GS | I_DS |
| ---- | ---- | ---- |
| Low  | Low  | Low  |
| Low  | High | Low  |
| High | Low  | Low  |
| High | High | High |

One final step: combining MOSFETS to create high and low voltage **outputs**, based on high and low voltage inputs. 

General approach: create transistor circuits that make current flow to outputs from high or low voltage, based on transistor input values.

---

# Index / Terminology

[^Protons]: (+) positive charge
[^Neutrons]: no charge
[^Electrons]: (-) negative charge
[^High electrical potential]: (-) many electrons
[^Low electrical potential]: (+) not many electrons
[^Voltage]: electrical potential (how many electrons)
[^Current]: rate of electron flow
[^Resistance]: measured in ohms ($\Omega$). $R=\cfrac{V}{I}$. Represent the relationship between voltage (**V**) and current (**I**)
[^Insulators]: high resistance, don't conduct electricity at all (narrow, twisty pipe)
[^Conductors]: low resistance, conduct electricity well (wide, smooth pipe)
[^Semiconductors]: somewhere in between conductors and insulators. It doesn't conduct electricity naturally. It depends on factors like temperature and **impurities**[^Doping] in the material.
[^Doping]: introduce impurities in the fabrication process, to increase the number of free charge carriers. 
[^Batteries]: a concentration of particles stored inside them up that will run out eventually (like water reservoirs)
[^Ground]: zero voltage point (region of low electrical potential)
[^Circuits]: Each of these circuits has a source of electrical particles, some path between this source and the ground, and some resistance along this path that dissipates these electrons.