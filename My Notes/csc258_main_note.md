---
date: 2017-10-24
tags: CSC258
---

# CSC258 Notes

[TOC]

---

## Overview

**Links**

-   [2008 CSC258 Page](http://www.teach.cs.toronto.edu/~ajr/258/) (very useful notes)
-   [Sample Midterm Questions](./csc258_sample_midterm.md)
-   [Boolean Basics](./boolean.md)
-   [Transistors](./transistors.md)
-   [Sequential Circuit](./sequential.md) 
-   [Arithmetic Logic Unit](./alu.md) 
-   [Verilog Basics](./verilog.md) 

**Contact**

| Name         | Office | Phone          | Email                                |
| ------------ | ------ | -------------- | ------------------------------------ |
| Steve Engels | BA4266 | (416) 946-5454 | sengels@cs.toronto.edu               |
|              |        |                | Mon 12pm - 1pm, Fri 12pm - 1pm       |
| Jongsok Choi | BA7172 | N/A            | jongsok.choi@mail.utoronto.ca        |
|              |        |                | Tue 9pm – 10pm, Fri 9:30am - 10:30am |

**Mark**

| Component    | Weight                                   |
| ------------ | ---------------------------------------- |
| Labs         | 28% (7 total, 4% each)                   |
| Project      | 14% (2% proposal + three 4% demos)       |
| Midterm exam | 18%                                      |
| Final exam   | 40%  ➔ you must get 40% on the final to pass the course |

**Course Structure**

```mermaid
graph LR
A[Transistors]
B[Gates]
C[Circuits]
D[Devices]
E[Flip-flops]
F[ALU]
G[FSM]
H[Processors]
I[Assembly Language]
A --> B
B --> C
C --> D
C --> E
D --> F
E --> G
F --> H
G --> H
H --> I
```

**A more comprehensive structure**

-   user command language
-   application program
-   toolkits / libraries
-   high-level language
-   machine language
-   microcode
-   CPU microarchitecture
-   registers and such
-   latches, flip-flops
-   combinational circuits
-   gates
-   transistors
-   electricity

**Evaluations**

-   **Transistors**
    -   Properties of electricity & understand basic principles of electric circuits.
    -   Illustrate how doped[^doping] semiconductormaterials can be configured to create transistors (p-n junctions).
    -   Combine transistors to create logic gates.
-   **Circuits**
    -   Create a truth table that represents the behaviour of a circuit you want to create.
    -   Translate them in terms from a truth table into gates that implement that circuit.
    -   Use **Karnaugh maps** to reduce the circuit to the minimal number of gates.

---

## Transistors

Connect point A to point B, based on the value at point C.

>   See more [Transistors](./csc258_transistors.md).

---

## Gates

### Making gates

Digital logic gates (e.g. AND, OR, NOT) are created by a **combination** of [transistors](#transistors).

### Gate Facts

-  “High” input = 5V, “Low” input = 0V
-  Switching time $\approx$ 120 picoseconds, switching interval $\approx$ 10 ns
-  NAND is most common logic gate

### Gate Logic (Boolean)

>   See more [Boolean Basics](./csc258_boolean.md).

A classic question is how to implement a NOT gate from a 2-input NAND gate?

---

## Circuits

### Making circuits

1.  Create truth tables.
2.  **(Key) Express as boolean expression.**
3.  Convert to gates.

### Minterms and Maxterms

-   **$m_x$ Minterm** = 
    -   an **AND** expression with every **input** present in true or complemented form. 
    -   A minterm indicates a set of inputs that will make the **output** go **high**. 
        ($m_0(\overline{A}\cdot \overline{B}\cdot\overline{C})$ to $m_7(A\cdot B \cdot C)$)
-   **$M_x$ Maxterm** = 
    -   an **OR** expression with every input present in true or complemented form. 
    -   A maxterm indicates a set of inputs that will make the **output** go **low**. 
        ($M_0(A+B+C)$ to $M_7(\overline{A}+\overline{B}+\overline{C})$)

The $x$ subscript indicates the row in the truth table. Given n inputs,there are $2^n$ minterms and maxterms possible (same as rows in a truth table).

==Any minterm is the **complement** of a (unique) maxterm.== 
e.g., $m_o =A’B’$ while $M_o =A+B$. 
(i.e. SOM and POM are equivalent.)

### Boolean Expressions

>   See more [Boolean Basics](./csc258_boolean.md).

There are 2 canonical forms of boolean expressions:

-   **SOM** Sum-of-Minterms
    Since each minterm corresponds to a single high output in the truth table, the combined high outputsare a **union** of these minterm expressions. 
    **Useful** when expressing **very few high** output cases.


-   **POM** Product-of-Maxterms
    Since each maxterm only produces a single low output in the truth table, the combined low outputs are an **intersection** of these maxterm expressions.
    **Useful** when expressing **very few low** output cases.


### K-Maps

= "Karnaugh maps".

##### General Rules:

-   Boxes must be rectangular, and aligned with map.
-   Number of values contained within each box must be a power of 2.
-   Boxes may overlap with each other.
-   Boxes may wrap across edges of map.

##### “Don’t care” values

-   Input values that will *never happen* (1010 input for a 0-9 seven segment decoder) or are *not meaningful* (tempreture is either hot or cold for an air condition controller) in a given design, and so their output values do not have to be defined.
-   Recorded as ‘X’ in truth-tables and K-Maps.
-   In the K-maps we can think of these don’t care values as either 0 or 1 depending on what helps us simplify our circuit.

---

## Devices

### Sequential Circuits

Circuits that depend on both the **current** inputs and the **previous state** of the circuit.

>   See more [sequential circuits](./sequential.md).

### Combinational Circuits

Combinational Circuits are any circuits where the outputs rely strictly on the **current** inputs. 

#### Multiplexers (MUX)

$M = Y·S + X·\overline{S}$ 

#### Decoders

Decoders are essentially translators. Translate from the output of one circuit to the
input of another. e.g. Demultiplexers, 7-seg decoder

##### Seven-segment decoders

4-digit binary number to the seven segments of a digital display. Turning a segment **on** involves driving it **low**. 

#### Adders (half and full), Subtractors

>   See more [arithmetic](./alu.md).

#### Comparators

##### Idea

-   A==B: $AB+\overline{A}\overline{B}$
    If A and B are 2 bits long, make sure that the values of bit 1 are the same and  the values of bit 0 are the same.
-   A>B: $A\overline{B}$, A<B: $\overline{A}B$ 
    If A and B are 2 bits long, Check if first bit satisfies condition. If not, **check that the first bits are equal** and then do the 1-bit comparison.

##### General Comparators (n-bit)

-   A==B: $X_0X_1...X_n$ for $X_i=A_iB_i+\overline{A}_i\overline{B}_i$ 
-   A>B: $A_n\overline{B_n}+X_nA_{n-1}\overline{B_{n-1}}+...+A_0\overline{B_0}\prod_{k=1}^nX_k$ 

---

## Flip-flops

>   See [sequential circuits](./sequential.md).

---

## Arithmetic Logic Units

>   See [arithmetic](./alu.md).

---

## Finite State Machines

A Finite State Machine is an abstract model that captures the operation of a sequential circuit. A FSM is defined (in general) as:

-   A finite set of states,
-   A finite set of transitions between states, triggered by inputs to the state machine,
-   Output values that are associated with each state or each transition (depending on the machine),
-   Start and end states for the state machine. 

So when designing FSM, there are questions we can ask:

-   What are the inputs?
-   What are the states of this machine?
-   How do you change from one state to the next?

Usually our FSM has more than one input, and will trigger a transition based on certain input values but not others. Also it might have input values that don’t cause a transition, but keep the circuit in the same state (transitioning to itself).

### Designing with flip-flops

Sequential circuits are the basis for memory, instruction processing, and any other operation that requires the circuit to remember past data values. These past data values are also called the **states** of the circuit. Sequential circuits use combinational logic to determine what the next state of the system should be, based on the past state and the current input values.

#### Counters as example

With counters, each state is the current number that is stored in the counter. On each clock tick, the circuit transitions from one state to the next, based on the inputs. 

### FSM Design Steps

1.  Draw state diagram

2.  Derive state table from state diagram

3.  Assign flip-flop configuration to each state

    Number of flip-flops needed is: $\lceil \log_2 (\text{# of states}) \rceil$ 

    -   One flip-flop => 2 states (0, 1)
    -   Two flip-flops => 4 states
    -   Three flip-flops => 8 states
    -   …
    -   Eight flip-flops => 2^8^ = 256 states

4.  Redraw state table with flip-flop values

5.  Derive combinational circuit for output and for each flip-flop input.

#### Moore Machine v.s. Mealy Machine

Two ways to derive the circuitry needed for the output values of the state machine:

-   **Moore machine:** 
    -   The output for the FSM depends **solely** on the current state (based on entry actions).
-   **Mealy machine:** 
    -   The output for the FSM depends on the state and the input (based on input actions). 
    -   Being in state X can result in different output, depending on the *input that caused that state*.

### Timing and state assignments

When assigning states, you need to consider the issue of timing with the states. For example, if recognizer
circuit is in state 011 and gets a 0 as an input, it moves to state 110.

-   It requires the first and last digits change “at the same time”
-   If the first flip-flop changes first, the output would go high for an instant, which could cause unexpected behaviour. (racing)

**Two possible solutions:**

1. Whenever possible, make flip-flop assignments such that neighbouring states differ by **at most one** flipflop value. Intermediate states can be allowed if the output generated by those states is consistent with the output of the starting or destination states.
2. If the intermediate states are unused in the state diagram, you can set the output for these states to
  provide the output that you need. You might need to add more flip-flops to create these states.

---

# Index / Terminology

[^Protons]: (+) positive charge
[^Neutrons]: no charge
[^Electrons]: (-) negative charge
[^High electrical potential]: (-) many electrons
[^Low electrical potential]: (+) not many electrons
[^Voltage]: electrical potential (how many electrons)
[^Current]: rate of electron flow
[^Resistance]: measured in ohms ($\Omega$). $R=\cfrac{V}{I}$. Represent the relationship between voltage (**V**) and current (**I**)
[^Insulators]: high resistance, don't conduct electricity at all (narrow, twisty pipe)
[^Conductors]: low resistance, conduct electricity well (wide, smooth pipe)
[^Semiconductors]: somewhere in between conductors and insulators. It doesn't conduct electricity naturally. It depends on factors like temperature and **impurities**[^Doping] in the material.
[^Doping]: introduce impurities in the fabrication process, to increase the number of free charge carriers. 
[^Batteries]: a concentration of particles stored inside them up that will run out eventually (like water reservoirs)
[^Ground]: zero voltage point (region of low electrical potential)
[^Circuits]: Each of these circuits has a source of electrical particles, some path between this source and the ground, and some resistance along this path that dissipates these electrons.