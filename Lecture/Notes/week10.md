# Week 9 Review
- Question #1
	- Recall the 3-step "basic approach"
	- Step #1: Data source and destination
	- Step #2: Determine the data path
	- Step #3: 
		- `Read` & `Write` signals
		- Data path signals
		- Other signals
- Question #2
- Question #3: load(=read) a memory value into $t0
- Question #4: assembly instructions
	- `sub $t7, $t0, $t1`
	- `andi $t7, $t0, 15`
	- `sra $t2, $t1, 2` (shift arithmetic)
- Question #5: assembly => machine code
	- `add $t7, $t0, $t1` (assembly)
	- type? R-type
		- 000000(r-type) sssss ttttt ddddd xxxxx 100000(add)
	- register values? remember temporary regs start at register 8
		- $t0 => 8(5'b01000), ...
		- don't need to memorize tho
		- 000000 01000 01001 01111 xxxxx 100000

---
# Lecture
starting from p43

## Control flow in assembly
We need non-linear instructions. Some require branch (i.e. if/else). Some require jump back and repeat (for/while).
For this, we need **labels**.

### Branch instructions
- similar to i-type instruction (so, 16-bits)
- `beq` (branch equal), `bgtz` (branch>0), `blez` (branch<0), `bne` (branch not equal).
- Syntax is `$s, ($t), label`
- labels are actually memory locations, assigned to each label when compile (offset, talk about it later)
- note that there's no branch compare immediate. So move the value into register first

#### `i` value
- `i` is an offset between the location (memory address) of the current instruction and the target of the branch (measured in # of instructions, not # of bytes). The instruction the processor should fetch next is i instructions before this current branch instruction (if i is negative) or i instructions after (if i is positive).
- Depending on the implementation (e.g., which cycle the PC+4 for each instruction takes place), i is computed as:
	- `(label location - (current PC)) >> 2`, 
	- or `(label location - (current PC + 4)) >> 2`

#### Conditional Branches
- branch is taken => good
- branch is not taken => `PC+4`
- Q: how far can a processor branch?
	- signed 16-bits = -2^15 to 2^15 -1
	- so, branches can go -32768 back & 32767 forward

### Jump instructions
- `j` and `jal` are j-type. `jalr` and `jr` are r-type since it works with registers.
- `j` (jump), `jal` (jump & link, used when there's a function call, since `PC+4` is saved to the `$ra`=return address), `jalr` (jump and link register), `jr` (jump )

### Comparison instructions
- `slt` , `sltu` , `slti`, `sltiu`

### If/Else in MIPS
- either way works:
	- `beq ... IF`
	- or `bne ... ELSE`
- trick: use flow chart to visualize the code

### Multiple if
- case 1: if (i==j || i==k)
- case 2: if (i==j && i==k)

### Loops in MIPS
- while loops
- for loops

### Interacting with memory
- i-type instruction `lw $t0, 12($s0)`

#### Loads / Stores
- Load (= read) from memory address to a register
- Store (= write) from register to memory address
- instructions: `lb`, `lbu`, `lh`, `lhu`, `lw`, `lb`, `sb`, `sh`, `sw`

#### Alignment

#### Little endian / Big endian

#### More about memory
- memorymappedIO
- trap / syscall function

### Memory segment syntax

## Pseudo Instructions

