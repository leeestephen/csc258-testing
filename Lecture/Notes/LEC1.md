# Intro to Digital Logic

-   first lab next thursday

Course outcomes:

-   Understandtheunderlyingarchitecture of computersystems.
-   Learn how to use this architecture to store data and create behaviour.
-   Usetheprinciplesofhardware design to createdigital logic solutions togiven problems.

**EX1**

**EX2**

why signed int range from -2^31? 

**Gates**

AND, OR, NOT, XOR, NAND(cheapest), NOR, Buffer

**Expressing digital logic**

Givenalogicproblemor circuit, truth tablesare used to describe itsbehavior

| A    | B    | C    | Y    |
| ---- | ---- | ---- | ---- |
| 0    | 0    | 0    | 1    |
| 0    | 0    | 1    | 1    |
| 0    | 1    | 0    | 1    |
| 0    | 1    | 1    | 1    |
| 1    | 0    | 0    | 1    |
| 1    | 0    | 1    | 1    |
| 1    | 1    | 0    | 1    |
| 1    | 1    | 1    | 0    |

# Transistors

**Goal**

-   Understand basic principles of electric circuits
-   Illustrate how doped semiconductor materials canbe configured to create transistors
-   Combine transistors to create logic gates

**Intro to Transistors**

Vacuum tube -> Transistors. Moore's Law. 

**Transistor Basics**

Transistors connect Point A to Point B, based on the value at Point C.

**Electricity Basics**

-   flow of charged particles (mostly electron)
-   atom = proton + neutron + electron
-   electrical potential
-   voltage
-   current
-   resistance - flow
-   sources of electricity
    -   batteries (water reservoirs)
    -   outlets (waterfall)
-   path of electricity
    -   ground - zero voltage point of a circuit
-   Using electricity
    -   circuits = source + path + resistance
-   Resistance(ohms, $\Omega$): insulators | conductors | semiconductors

**Semiconductors**

-   Impurities
-   n-type
-   p-type
-   p+n
-   p-n Junctions
    -   depletion layer
    -   electric field
    -   Forward Bias (+p-n) -> short-circuit
    -   Reverse Bias (-p+n) -> open-circuit
-   Electric fields
    -   drift
    -   diffusion
    -   equilibrium (not conducted)
-   Creating Transistors
    -   BJT
    -   MOSFET
    -   JFET
-   MOSFET
    -   MO
    -   S
    -   n-channel
    -   nMOS, pMOS
-   Transistors -> Gates

# Lab1 - Preparation

