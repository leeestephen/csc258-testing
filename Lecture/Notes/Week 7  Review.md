# Week 7  Review

**Q1** Multiplication

**Q2** Operation selection

**Q3** ALU: Arithmetic + Logic

---

# Processor Continued

## Storage Unit

-   Register file (books)
-   Main memory (library)
-   Others: 
    -   cache (local library branch, speed between register and main memory), and 
    -   networks (collections around the world)

### Register file

-   Read
-   Write

### Memory

-   m Rows \* n bits => 2^m^ * n bits

#### Storage cells

Each row is made of n storage cells.

Each cell stores a single bit.

-   RAM cell
-   DRAM IC cell

### Decoder

**One-hot decoder**: Only one bit is high. (=> activates a single row)

Advantage: 

### Data bus

#### Tri-state buffer

### RAM Memory Interface

#### Timing constraints

### Memory vs Registers

---

## Control Unit

-   datapath
-   contorl unit

### Instruction Execution

### Instructions

### Program Counter

PC stores the location (memory address) of the current instruction.

#### Update PC

### Decode Instructions

-   Instruction Set Architecture
-   MIPS ISA

#### Instruction registers

-   Opcodes - top 6 bits
-   R-type instructions
    -   All operations happen between registers

#### MIPS ISA

##### Instruction types

-   R
-   I
-   J

### Control Unit Signals

---

# Lab 7

