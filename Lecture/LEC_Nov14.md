# Announcements

- projects starting next week
- plan to open up lab on Monday Dec 4th 6-9pm

# Week 8 Review

## Instructions

- 32-bit bin numbers:
- assembly language instruction: human-readable format

### Decoding

- Example: `subu $d[estination], $s[ource], $t[arget]` => 000000 00000 00001 00111 00000 100011
	- this is r-type instruction
	- top 6 bits always 0 (opcode)
	- bottom 6 bits indicate non sign operation for r-type
	- So this means `Reg 7 = Reg 0 - Reg 1`
- MIPS ISA
	- Types:
		- R-type
			- 6 opcode, 5 rs, 5 rt, 5 rd, 5 sh[ift]am[oun]t, 6 funct
			- reg-to-reg operation: 2 source regs (rs & rt), 1 destination reg (rd)
			- opcode is `000000`.
		- I-type
			- deal with constant value (immediate value)
			- 6 opcode, 5 rs, 5 rt, 16 immediate value
		- J-type
			- deal with jumps to some address in the program (e.g. function calls)
				- jLABEL
			- 6 opcode, 26 address (disregard buttom 2 bits, which are all 0s, more detail later)
			- 26-bits target address
				- position 1 and 0 always 0 since instructions are word-aligned
				- 27 - 2 are the 26 bits provided in the instruction
				- the first four bits of the destination add are the same as the current
				- So we cannot jump more than 256MB in memory
					- use Linker
					- or use register, `jr` instruction, more on later
	- Opcodes

## Q1: conversion

- (careful!) 6 address bits <=> 2^6^=64 memory slots <=> 64 bytes
- 32-bit arch => 4 bytes per integer
- So 16 integers

## Q2: timing

- interval #1: address setup time
	- must be stable before enabling write signal
- interval #2: data setup to write end
	- time for data to be set-up at destination
- interval #3: data hold from write end
	- data should stay unchanged after wrter signal changes

## Q4: short questions

- where instrucions and data stored?
	- memory
- how long is a single instruction?
	- 4 bytes
- what is the role of program counter?
	- store the locatoin of the current instruction
- what is instruction fetch?
	- retrive from memory

# Handout

...

# Assembly language

## Controlling the Datapath
- MIPS Datapath
	- how to do the following?
- Signals control

### Basic Approad to datapath
1. figure out data sources and destination
2. determine the path for data
3. deduce the signal values that cause this path:
	- Start with Read & Write signals (at most one can be high at a time).
	- Then, mux signals along the data path (unlike Read & Write, when we don't use them, we set them to `X`).
	- Non-essential signals get an X value.

### Example #1: incrementing PC
result is in the slide.

### MIPS datapath notes
- Jump instructions

## Machine code instructions

### Assembly language
- each processor type has different code words
- machine code <=> assembly language
	- 1-to-1 mapping

### Machine code + registers (`$x`)
- MIPS is reg-to-reg
- MIPS provides 32 registers (32-bits)
	- Register0($zero):value0--always.
	- Register1($at):reservedfortheassembler.
	- Registers2-3($v0,$v1):returnvalues
	- Registers4-7($a0-$a3):functionarguments
	- Registers8-15,24-25($t0-$t9):temporaries
	- Registers16-23($s0-$s7):savedtemporaries
	- Registers28-31($gp,$sp,$fp,$ra):memoryandfunction support
	- Registers26-27:reservedforOSkernel
	- Register PC:, not directly accessible.
	- Register HI: top 32 bits used in multiplication and division, not directly accessible.
	- Register LO: bottom 32 bits used in multiplication and division, not directly accessible.
- Multiplication and HI/LO
- Division and HI/LO

### Machine code details
- "don't care" bits => `X` but assembly interpreter always assigns them to `0`

## Assembly language instructions

### MIPS 
- RISC vs CISC

### MIPS instructions

### Arithmetic instructions
add addu addi addiu div divu mult multu sub subu

- R-Type: add addu div divu mult multu sub subu
- I-Type: addi addiu

### Logical instructions
and andi nor or ori xor xori

### Shift instructions
s[hift]l[eft]l[ogical] sllv[ariable] sra[rithmetic] srav srl srlv

- Logical shift vs arithmetic shift

### Data movement instructions
mfhi mflo mthi mtlo

(actually copy the data)

### ALU instructions
- most are R-type

### Example program: Fibonacci sequence
1, 1, 2, 3, 5, 8, 13, ...

C code:

```
int fib(void) {
   int n = 10;
   int f1 = 1, f2 = -1;

   while (n != 0) {
      f1 = f1 + f2;
      f2 = f1 – f2;
      n = n – 1;
	}
	return f1; 
}
```

Assembly code example:

```
# fib.asm
# register usage: $t3=n, $t4=f1, $t5=f2
#
FIB:  addi $t3, $zero, 10	# initialize n=10
      addi $t4, $zero, 1	# initialize f1=1
      addi $t5, $zero, -1	# initialize f2=-1
LOOP: beq $t3, $zero, END 	# beq meas branch equal; done loop if n==0
      add $t4, $t4, $t5 	# f1=f1+f2 
      sub $t5, $t4, $t5		# f2=f1-f2
      addi $t3, $t3, -1		# n = n – 1
      j LOOP 				# j[ump] to LOOP; repeat until done
END:  sb $t4, 0($sp)		# store result
```

## Simulating MIPS

