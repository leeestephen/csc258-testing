### Textbook Readings

Below are the readings that correspond to the topics of the course.

Week 1: Transistors

Section 6-1, p295-304   (Mano & Kime)

Week 2: Combinational Circuit Design

Sections 2-1, 2-2, 2-3, 2-4, 2-5, 2-6, p35-72   (Mano & Kime)

Week 3: Logic Devices

Sections 3-1 to 3-9, 4-1 to 4-4   (Mano & Kime)

Week 4: Sequential Circuits

Sections 5-1 to 5-6   (Mano & Kime)

Week 5-6: Sequential Circuits and Finite State Machines

Section 7-6, 7-12, 5-7 & 5-9   (Mano & Kime)

Week 7-8: Processors

Sections 7-7, 7-8, 8-1 to 8-4, 9-1 to 9-6, 2-6, p35-72   (Mano & Kime)

Week 9-12: Assembly Language

http://www.mrc.uidaho.edu/mrc/people/jff/digital/MIPSir.html

http://programmedlessons.org/AssemblyTutorial/index.html