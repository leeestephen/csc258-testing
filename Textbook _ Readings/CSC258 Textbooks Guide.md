# CSC258 Textbooks

Here are some textbooks you may wish to <u>use for reference</u> during this course. Most of them are in the U of T library system. There is no required textbook; you can get all you need in this course from lectures, tutorials, labs, assignments, other handouts, and thinking.

-   M.M. Mano and C.R. Kime,

    ***Logic and Computer Design Fundamentals***

    , various editions (I've seen the second (2000/2001) and fourth (2008)), Prentice-Hall.

    >   A very good match for this course in terms of content and sequence. A commonly-used textbook for this course. Some things in the second edition were annoying, but they seem to have been fixed or omitted in the fourth edition. I recommend either **this** or the **Hamacher** et al book as a textbook for this course. Or **Heuring**.

-   V. Carl Hamacher, Zvonko G. Vranesic, and Safwat G. Zaky. 

    ***Computer Organization***

     (3rd, 4th, or 5th edition), McGraw-Hill, 1990, 1996, or 2002 (respectively).

    >   A common textbook for this course. Its content is a fairly good match for this course; it has a bit more of an engineering orientation, which for this course is definitely a good thing. One problem with this textbook for this course is that we spend about a third of the course doing material all of which is in appendix A. (Actually, I'm not so sure that that's so terrible, which is why I've specified it as the standard textbook many times.) It's a better match for this course than the Mano 1993 book in terms of level, but it's a worse match in terms of sequence and organization.
    >
    >   The second and third editions discuss PDP-11 addressing but the subsequent editions do not.
    >
    >   (I guess there's likely a subsequent edition by now, but I haven't seen it.)

-   Heuring and Jordan, 

    ***Computer Systems Design and Architecure***

    , second edition, Pearson / Prentice-Hall, 2004.

    >   I'm not sure what to write here about this book. It's pretty good. It covers the material in a top-down sequence rather than bottom-up, which is one of the reasons that I used it when I did CSC 258 in that sequence in 2005. But we're doing the course in bottom-up sequence this time because I've concluded that that works better.

-   M. Morris Mano.

    *Computer Engineering: Hardware Design*

    . Prentice-Hall, 1988.

    >   Contains a gentler introduction to sequential circuits than our textbook. I don't like the way he does Karnaugh maps, which I find not to be very clear. The presentation of combinational circuits is basically the same, and only about half the course material is covered by this book. I suggest using this as a reference only for sequential circuits (flip-flops and counters and all), and only if you find you want a gentler introduction.

-   M. Morris Mano,

    *Computer System Architecture*

    (3rd edition), Prentice-Hall, 1993.

    >   A fairly good match for this course in terms of content and sequence, but too low-level in parts. (I find the way he does Karnaugh maps annoying and not particularly clear, but that is just one small portion of the book.) I've used this book as the course textbook in the past.

-   M. Morris Mano.

    *Digital Design*

    (second or third edition). Prentice-Hall, 1991 or 2002 (respectively).

    >   Similar to other books by Mano (there are rather a lot of them, it seems),*except* that chapter 10 contains an introduction to gate construction, more detailed than will be presented in this course, but requiring much less physics background than Fortney. Actually, it's too bad he didn't also include some of the quantum-mechanical physics stuff about N- and P-type semiconductors, because that isn't *so* hard... so after reading this chapter 10, you might want to go to the Fortney book.

-   Lloyd R. Fortney.

    *Principles of Electronics: Analog and Digital*

    . Harcourt Brace Jovanovich / Academic Press, 1987.

    >   Not relevant to most of this course, but if you would like to learn something about how gates are constructed, this is a definitive reference. Among many other topics, it contains a detailed description of the use of semiconductors and the diode effect, transistor design, and the implementation of logic gates. Requires some atomic physics; I think it's intended as a textbook for a second-year physics course in electronics. However, since you're not going to be examined on this material, you might find it of interest and *mostly* intelligible even if your physics background is weak.

-   William Stallings. 

    *Computer Organization and Architecture*

    (fourth edition). Prentice-Hall, 1996.

    >   While not a suitable textbook for this course, we will be doing computer arithmetic roughly in accordance with his approach in his chapter 8, so this is a good additional reference for computer arithmetic and integer representation.

-   Gideon Langholz, Abraham Kandel, and Joe L. Mott.

    *Digital Logic Design*

    . Wm C. Brown Publishers, Dubuque, Iowa.

    >   Contents: Integer representation and arithmetic, boolean algebra, combinational circuits and Karnaugh maps, sequential circuits. 
    >   Very careful derivations and definitions of boolean algebra concepts we need such as sum-of-products forms. Rigorous development of and description of Karnaugh map methods. (Nothing special wrt number representation and arithmetic.)

-   John L. Hennessy and David A. Patterson. 

    *Computer Organization and Design: The Hardware/Software Interface*

    . Morgan Kaufmann, 1994.

    >   Another textbook for this sort of course, but with a fairly different approach. Starts with RISC CPUs from the beginning. More programming-intensive than this course. You may enjoy reading it to get more of a feel for RISCs (probably not discussed until the last week of our course), although it's a long book for just that.

-   Yale N. Patt and Sanjay J. Patel.

    *Introduction to Computing Systems: From Bits and Gates to C and Beyond*

    . McGraw-Hill, 2001.

    >   This is a really neat book which attempts to teach a large swath of a basic first- and second-year practical curriculum (not just computer organization; quite engineering-oriented though), entirely from the bottom up. Thus it starts with CSC 258ish material, and then uses the computer which is developed in the first bit to show how the C programming language could be implemented, and teaches C, and some first-year programming concepts such as recursion.
    >
    >   Unfortunately, it doesn't cover enough of the basic computer organization material to be a suitable official textbook for this course. CSC 258 students might find this book particularly helpful in understanding some of the OS-related material (interrupts and such), but that's a fairly small portion of this course. CSC 209 students who have taken CSC 258 might find this book interesting in its discussion of how C relates to the low-level machine, to the extent that this isn't obvious after completing CSC 258 and learning C.

