### HDLBits: An Online Tool w/ Practice Problems in Verilog

HDLBits is an online tool, created by Henry Wong, that we think can be very useful in practicing your Verilog and hardware design skills!

"*HDLBits is a collection of small circuit design exercises for practicing digital hardware design using Verilog Hardware Description Language (HDL). *

*Each problem requires you to design a small circuit, implemented in Verilog HDL. HDLBits gives you immediate feedback on the circuit module you submit. Your circuit is simulated in parallel with our reference solution. Using a set of test vectors, the outputs of the modules are compared, telling you whether your circuit functioned correctly. Some practice problems will also generate a timing diagram that can show a cycle-by-cycle comparison of your circuit to the reference solution.*"

You can access HDLBits here: <http://verilog.stuffedcow.net/wiki/Main_Page>

A few things to note:

-   Even though we have tested quite a few of the problems ourselves, please note that this is a 3rd party tool that is **not** maintained by us. Therefore, we will not be providing support for any infrastructure issues that might arise. For those, you may contact the tool creator, but please be aware that this is **not** a full-time managed project.

​           You are of course welcome to ask us any Verilog-related questions that come up while using this online tool!

-   By the end of the 4th week of classes, you should be able to complete all problems in the Combinational Problem set: <http://verilog.stuffedcow.net/wiki/Problem_sets#Combinational_Logic>

**Happy Verilog coding!**