### Verilog Tutorial Videos - from ECE241 Fall 2015

You may find these Verilog tutorial videos helpful!

These tutorial sessions were run in Fall 2015 by Xander Chin in the context of a different course (ECE241) that also used Verilog in the labs. These tutorial sessions were recorded; you can find the links to these videos below.

Since the tutorial period was interactive, there can be some bits in the middle where Xander is answering questions that you'll probably want to skip over. If you use Chrome to view the youtube links, you can set playback to 1.5x to speed things along (this feature might not be available using other browsers).

Tutorial #1: Verilog - Basic syntax, Bitwise operators, Hierarchy, Simulation. <https://youtu.be/uWp4MuX2NqQ>

Tutorial #2: Verilog - More Operators, Combinational Always blocks. <https://youtu.be/7YhSPLiU2XI>

Tutorial #3: Verilog - Always blocks review, Sequential Always blocks. <https://youtu.be/sUz2SGeNe90>